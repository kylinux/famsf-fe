var FAMSF = {};

(function($) {

  FAMSF = {
    $document: $(document),
    $window: $(window),
    $body: $('body'),
    // All pages
    'common': {
      init: function () {
        // Events Handler
        svg4everybody();
        FAMSF.eventsHandler.init();
        FAMSF.mediaOverlay.init();

        if ($('.js-header').length)               { FAMSF.header.init(); }
        if ($('.js-ticker').length)               { FAMSF.ticker.init(); }
        if ($('.js-carousel-marquee').length)     { FAMSF.carouselMarquee.init(); }
        if ($('.js-manifesto').length)            { FAMSF.manifesto.init(); }
        if ($('.js-newsletter').length)           { FAMSF.newsletter.init(); }
        if ($('.js-map').length)                  { FAMSF.map.init(); }
        if ($('.js-permanent-collections').length){ FAMSF.permanentCollections.init(); }
        if ($('.js-media-gallery').length)        { FAMSF.mediaGallery.init(); }
        if ($('.js-cinematic-gallery').length)    { FAMSF.cinematicGallery.init(); }
        if ($('.js-editorial').length)            { FAMSF.editorial.init(); }
        if ($('.js-visit-notification').length)   { FAMSF.visitNotification.init(); }
        if ($('.js-body-module').length)          { FAMSF.bodymodule.init(); }
        if ($('.js-bio').length)                  { FAMSF.bio.init(); }
        if ($('.js-blocks').length)               { FAMSF.blocks.init(); }
        if ($('.hero--collection').length)        { FAMSF.collectionHero.init(); }
        if ($('.join-give-intro').length)         { FAMSF.joinGiveIntro.init(); }
        if ($('.js-membership-level').length)     { FAMSF.membershipLevel.init(); }
        if ($('.js-events-filter').length)        { FAMSF.eventsListing.init(); }
        if ($('.js-calendar').length)             { FAMSF.calendar.init(); }

        FAMSF.figures.init();
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        FAMSF.transition.init();
        FAMSF.drag.init();
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = FAMSF;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

  jQuery.fn.reverse = [].reverse;

})(jQuery); // Fully reference jQuery after this point.

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.bio = {
    init: function () {
      var $context = $('.js-bio');


      $.each($context, function(){
        var $this     = $(this);
        var $btn      = $('.js-bio-btn', $this);
        var $content  = $('.collection__body', $this);
        var $inner    = $('.collection__body-inner', $this);
        var _$inner   = { height : $inner.height() };
        var $gallery  = $('.collection__gallery', $this);

        var _label = {
          expanded  : 'Show Less -',
          collapsed : 'Show More +'
        };

        var getInnerHeight = function(){
          _$inner.height = $inner.height();
          $('.is-expanded', $this).css('max-height', _$inner.height);
        };

        // events
        $btn.on('click', function(e){
          e.preventDefault();

          if ( $content.hasClass('is-expanded')){
            $btn
              .text( _label.collapsed);
            $this
              .removeClass('is-expanded');
            $content
              .removeAttr('style')
                .removeClass('is-expanded');
            

            $("html, body")
              .animate({ scrollTop: $this.offset().top });
            
          } else {
            $btn
              .text( _label.expanded );
            $this
              .addClass('is-expanded');
            $content
              .css('max-height', _$inner.height)
              .addClass('is-expanded');
            
            
            $(window)
              .lazyLoadXT();
          }

          setTimeout(function(){
            FAMSF.transition.refresh();
          }, 800);
          
        });

        FAMSF.eventHandler.subscribe('EVENT_RESIZE_INSTANT', getInnerHeight);
      });
    }
  };
})(jQuery);
FAMSF = window.FAMSF || {};

(function ($) {
    FAMSF.bodymodule = {
        init: function () {
            var _cta = $('.showmore-cta');
            _cta.on('click',function() {
               if( $(this).is('.clicked')){
                   $(this).html('Show More +');
                   $(this).removeClass('clicked');
                   $(this).prev('.body-module-content').removeClass('open');
               } else {
                   $(this).addClass('clicked');
                   $(this).html('Show Less -');
                   $(this).prev('.body-module-content').addClass('open');
               }
            });
        }
    };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.calendar = {
    init: function () {
      FAMSF.calendar.loadCalendar();
      FAMSF.calendar.loadWaypoints();

      $(window).on('resize', function() {
        FAMSF.calendar.updateElements();
      });

      $('.js-calendar--fixed').on('click', '.js-calendar--open, .js-calendar--close', function(e) {
        e.preventDefault();
        FAMSF.calendar.toggle();
      });

      $(document).on('click', function(e) {
        if (!$(e.target).parents('.js-calendar--fixed').length &&
          !$(e.target).parents('.js-calendar--prev, .js-calendar--next').length &&
          !$(e.target).parents('.js-calendar--day').length) {
          $('.js-calendar--fixed').removeClass('calendar--open');
        }
      });
    },

    _loadCalendar: function($calendar) {
      var $calendarContainer = $('.js-calendar--container');
      var $calendarWrapper   = $('.js-calendar--wrapper');
      var $eventsListing   = $('.events-listing');

      return $calendar.clndr({
        template: $('#calender-template').html(),
        showAdjacentMonths: false,
        trackSelectedDate: true,
        daysOfTheWeek: ['S', 'M', 'T', 'W', 'Th', 'F', 'S'],
        targets: {
          nextButton: 'js-calendar--next',
          previousButton: 'js-calendar--prev'
        },
        clickEvents: {
          click: function(target) {
            if ($eventsListing.length) {
              var currentBreakpoint = FAMSF.utils.getBreakpoint();

              if ('small' === currentBreakpoint && $(target.element).parents('.js-calendar--fixed').length) {
                // Mobile should scroll to top
                $('html, body').animate({ scrollTop: 0 }, 200);
              } else if ('small' !== currentBreakpoint && $(target.element).parents('.js-calendar--inline').length) {
                // Tablet+ should scroll to top of Events Listing
                $('html, body').animate({ scrollTop: $('.events-listing').offset().top - 80 }, 200);
              }

              FAMSF.eventsListing.updateListing(target.date);

              // Mobile calendar should retract on date selection.
              if (currentBreakpoint === 'small') {
                $('.js-calendar--fixed').removeClass('calendar--open');
              }
            }

            FAMSF.calendar.selectedDate = target.date;

            // Updates calendar month view.
            if ($(target.element).parents('.js-calendar--fixed').length) {
              FAMSF.calendar.clndrInline.setMonth( FAMSF.calendar.selectedDate.month() );
              FAMSF.calendar.clndrInline.setYear( FAMSF.calendar.selectedDate.year() );
            }
            if ($(target.element).parents('.js-calendar--inline').length) {
              FAMSF.calendar.clndrFixed.setMonth( FAMSF.calendar.selectedDate.month() );
              FAMSF.calendar.clndrFixed.setYear( FAMSF.calendar.selectedDate.year() );
            }

            FAMSF.calendar.updateElements();
          }
        },
        doneRendering: function() {
          if ($eventsListing.length) {
            if ($calendar.find('.calendar__cell.today').length && typeof FAMSF.calendar.selectedDate === 'undefined') {
              var $today   = $calendar.find('.calendar__cell.today');
              var currentDay = moment();
              var classes  = $today.attr('class').split(' ');

              for (var i = 0; i < classes.length; i++) {
                if (classes[i].indexOf('calendar-day') > -1) {
                  currentDay = moment(classes[i].substr(-10), 'YYYY-MM-DD');
                }
              }

              $today.removeClass('today').addClass('selected');
              $('.js-current-date').html(currentDay.format('MMMM D, YYYY'));

              FAMSF.calendar.selectedDate = currentDay;
            }
          }

          FAMSF.calendar.updateElements();
        }
      });
    },

    loadCalendar: function() {
      this.clndrInline = this._loadCalendar( $('.js-calendar--inline').find('.js-calendar--wrapper') );
      this.clndrFixed  = this._loadCalendar( $('.js-calendar--fixed').find('.js-calendar--wrapper') );
    },

    toggle: function() {
      $calendar = $('.js-calendar--fixed');

      if ($calendar.hasClass('calendar--open')) {
        $calendar.removeClass('calendar--open');

        if (typeof FAMSF.calendar.clndrFixed !== 'undefined') {
          FAMSF.calendar.clndrFixed.setMonth( FAMSF.calendar.selectedDate.month() );
          FAMSF.calendar.clndrFixed.setYear( FAMSF.calendar.selectedDate.year() );
        }
      }
      else {
        $calendar.addClass('calendar--open');
      }
    },

    loadWaypoints: function() {
      $(window).on('load resize scroll', this.updateWaypoints);
    },

    updateWaypoints: function() {
      var $w               = $(window);
      var $calendar        = $('.js-calendar');
      var $calendarFixed   = $('.js-calendar--fixed');
      var $calendarWrapper = $calendarFixed.find('.js-calendar--wrapper');
      var $eventsListing   = $('.events-listing');

      // Tablet+.
      if (FAMSF.utils.getBreakpoint() !== 'small') {
        if ($('.events-listing-blocks .block--item').length === 0) {
          $calendarFixed
            .removeClass('calendar--show')
            .addClass('calendar--hidden');
          return false;
        }

        var headerHeight     = $('.js-header').outerHeight();
        var top              = (($w.height() + headerHeight) * 0.5) - ($calendarWrapper.outerHeight() * 0.5);
        var topTriggerPoint  = $calendar.outerHeight();
        var hideTriggerPoint = $eventsListing.offset().top + $eventsListing.outerHeight(true) - $calendarWrapper.outerHeight() - top;

        // Hides widget.
        if ($w.scrollTop() >= hideTriggerPoint || $w.scrollTop() < topTriggerPoint) {
          $calendarFixed
            .removeClass('calendar--show')
            .addClass('calendar--hidden');
        }
        // Sticks widget vertically centered and fixed.
        else  {
          $calendarFixed
            .removeClass('calendar--hidden')
            .addClass('calendar--show')
            .css({ 'top': top });
        }
      }
      // Mobile.
      else {
        $calendarFixed.removeAttr('style');
      }
    },

    updateElements: function() {
      if (FAMSF.calendar.selectedDate !== 'undefined') {
        // Removes modifier classes of current and selected date.
        $('.calendar__cell')
          .removeClass('today')
          .removeClass('selected');

        // Updates the date displayed on calendar widget.
        $('.js-calendar--date').html(FAMSF.calendar.selectedDate.format('MMMM D'));

        // Updates the date displayed on Events Listing filter.
        $('.js-events-filter--date').html(FAMSF.calendar.selectedDate.format('MMMM D, YYYY'));

        // Adds class of the selected date.
        var ymd = FAMSF.calendar.selectedDate.format('YYYY-MM-DD');
        $('.calendar-day-' + ymd).addClass('selected');
      }

      if (FAMSF.utils.getBreakpoint() !== 'small') {
        $('.js-calendar--inline, .js-calendar--fixed').each(function() {
          var $calendar = $(this);
          var wrapHeight = $calendar.find('.js-calendar--wrapper').outerHeight();
          wrapHeight   -= $calendar.hasClass('js-calendar--inline') ? $('.js-header').height() : 0;
          $calendar.find('.js-calendar--title').width( wrapHeight );

          $calendar.find('.js-calendar--nav').height( $calendar.find('.js-calendar--grid').height() );
        });
      }
      else {
        $('.js-calendar--title, .js-calendar--nav').removeAttr('style');
      }
    },

    resetCalendar: function() {
      if ($('.js-calendar').length === 0) {
        return false;
      }

      $('.js-calendar--fixed')
        .removeClass('calendar--open')
        .removeClass('calendar--show')
        .addClass('calendar--hidden');

      this.updateElements();
    },

    navigate: function(momentDate, direction) {
      this.navigateDirection = direction;

      if (direction === 'previous') {
        this.clndrInline.back();
        this.clndrFixed.back();

        $('.js-calendar--inline, .js-calendar--fixed').each(function() {
          $(this).find('[class*=calendar-day]').last().trigger('click');
        });
      }
      else if (direction === 'next') {
        this.clndrInline.forward();
        this.clndrFixed.forward();

        $('.js-calendar--inline, .js-calendar--fixed').each(function() {
          $(this).find('[class*=calendar-day]').first().trigger('click');
        });
      }
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.carouselMarquee = {
    init: function () {
      var $carouselMarquee      = $('.carousel-marquee');
      var $carouselMarqueeItems = $('.carousel-marquee__items', $carouselMarquee);

      new FAMSF.Slider($carouselMarqueeItems);

      if ($('.carousel-marquee__item', $carouselMarquee).length === 1) {
        $carouselMarquee.addClass('carousel-marquee--single');
      }
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
    FAMSF.cinematicGallery = {
        init: function () {

            var _gallery = $('.js-cinematic-gallery'),
                _galleryControls = $('.js-cinematic-gallery-caption'),
                _galleryItems = _gallery.find('.gallery-item'),
                _item_length = $('.js-cinematic-gallery > div').length,
                _item_length_index = _item_length - 1,

                $dataAttributes = $('.data-attributes'),
                _svgSpritePath = $dataAttributes.data('svgSpritePath'),
                _carouselArrowLeft = _svgSpritePath + '#' + $dataAttributes.data('carouselArrowLeft'),
                _carouselArrowRight = _svgSpritePath + '#' + $dataAttributes.data('carouselArrowRight');

            _gallery.on('init', function(event,slick) {
                $('.slick-cloned .media-overlay__item').remove();
            });

            _gallery.slick({
                lazyLoad: 'ondemand',
                infinite: true,
                arrows: true,
                slidesToShow: 1,
                asNavFor: _galleryControls,
                prevArrow: "<button class='slick-prev'><svg role='img'><use xlink:href='" + _carouselArrowLeft + "'></use></svg></button>",
                nextArrow: "<button class='slick-next'><svg role='img'><use xlink:href='" + _carouselArrowRight + "'></use></svg></button>"
            });

            _galleryControls.slick({
                infinite: true,
                arrows: false,
                fade: true,
                asNavFor: _gallery
            });


            $('.js-gallery--current').text( 1 );
            $('.js-gallery--total').text( _galleryItems.length );


            _gallery.on('beforeChange', function (event, slick, currentSlide) {

                if( _item_length_index == currentSlide) {
                    var _bgInject = $('.gallery-item[data-slick-index="1"]').attr('data-item');
                    console.log(_bgInject);
                    $('.slick-cloned[data-slick-index='+ _item_length +']').append('<div class="slideInject" style="background-image: url(' + _bgInject + '); "></div>');
                }
            });

            _gallery.on('afterChange', function (event, slick, currentSlide) {
                $('.js-gallery--current').text( currentSlide + 1 );
                $('.slick-cloned .media-overlay__item').remove();
                $(window).trigger('scroll');
                $('.slick-cloned').each(function() {
                    var $this  = $(this);
                    var $image = $this.attr('data-item');
                    $this.css('background-image', 'url("' + $image + '")');
                });
                $('.slideInject').remove();


            });


        }
    };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
    FAMSF.collectionHero = {
        init: function () {
            var collection = $('.hero--collection');
            var bgitems = collection.attr('data-item').split(',');

            var images = bgitems;
            $('.media-bg').css({'background-image': 'url('+ images[Math.floor(Math.random() * images.length)] + ')'});
        }
    };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.editorial = {
    init: function () {
      $.lazyLoadXT.scrollContainer = '.editorial__scroll';
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
    FAMSF.eventsListing = {
        init: function () {
            var $dateFilter = $('.js-events-filter');
            var $dateLabel = $dateFilter.find('.js-events-filter--date');

            $dateFilter.on('click', '.js-events-filter--nav', function(e) {
                e.preventDefault();

                var currentDay = moment($dateLabel.text(), 'MMMM-D-YYYY');
                var queueDay = moment($dateLabel.text(), 'MMMM-D-YYYY');
                var direction = $(this).hasClass('js-events-filter--prev') ?
                    'previous' : 'next';

                if (direction === 'previous') {
                    queueDay = queueDay.subtract(1, 'days');
                }
                else if (direction === 'next') {
                    queueDay = queueDay.add(1, 'days');
                }

                $dateLabel.text(queueDay.format('MMMM D, YYYY'));

                if ($('.js-calendar').length) {
                    if (currentDay.format('M') !== queueDay.format('M')) {
                        FAMSF.calendar.navigate(queueDay, direction);
                    }
                    else {
                        var ymd = queueDay.format('YYYY-MM-DD');
                        $('.calendar-day-' + ymd).trigger('click');
                    }
                }
            });

            if (/^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
                $(window).on('load resize', function() {
                    FAMSF.eventsListing.computeBlockHeight();
                });
            }
        },

        updateListing: function(momentDate) {
            // TODO: Do AJAX call here.

            // Dummy data.
            var blocks = [
                {
                  "url": "#",
                  "image": "img/fpo-4x3-4.jpg",
                  "category": "Special Events",
                  "title": "&ldquo;Bouquets to Art&rdquo;: Elegant Catered Lunch",
                  "dateVenue": "12:00pm – 1:45pm, Piazzoni Murals Room"
                },
                {
                  "url": "#",
                  "image": "img/fpo-4x3-5.jpg",
                  "category": "Special Events",
                  "title": "&ldquo;Bouquets to Art&rdquo;: Lecture &amp; Demonstration by Francois Weeks",
                  "dateVenue": "2:00pm – 3:30pm, Wilsey Court"
                },
                {
                  "url": "#",
                  "image": "img/fpo-4x3-6.jpg",
                  "category": "Docent Tour",
                  "title": "In Pursuit of Excellence: American Decorative Arts",
                  "dateVenue": "12:00pm – 1:30pm, Wilsey Court"
                },
                {
                  "url": "#",
                  "category": "Docent Tour",
                  "title": "&ldquo;Three Masterpieces in 30 Minutes&rdquo;",
                  "dateVenue": "1:00pm – 1:30pm, Wilsey Court"
                },
                {
                  "url": "#",
                  "category": "Docent Tour",
                  "title": "Art and Architecture: Hightlights of the de Young",
                  "dateVenue": "2:00pm – 3:00pm, Wilsey Court"
                },
                {
                  "url": "#",
                  "category": "Docent Tour",
                  "title": "Colonial Through Contemporary",
                  "dateVenue": "11:00am – 12:00pm, The Rotunda"
                },
                {
                  "url": "#",
                  "category": "Docent Tour",
                  "title": "Beyond Western Africa, Oceania, and the Americas",
                  "dateVenue": "12:00pm – 12:30pm, The Rotunda"
                }
            ];

            // Shuffle an array.
            function shuffle (array) {
              var i = 0;
              var j = 0;
              var temp = null;

              for (i = array.length - 1; i > 0; i -= 1) {
                j = Math.floor(Math.random() * (i + 1));
                temp = array[i];
                array[i] = array[j];
                array[j] = temp;
              }

              return array;
            }

            blocks = shuffle(blocks);

            // Prepare dummy response.
            var count = Math.floor( Math.random() * blocks.length );
            var response = { 'blocks': blocks.slice(0, count) };

            // TODO: Put the codes below inside the AJAX success method.
            $('.events-listing-blocks .block--error').removeClass('visible');

            if (response.blocks.length) {
                $('.events-listing-blocks .block--item').remove();

                for (var i = 0; i < response.blocks.length; i++) {
                    var $content = $( $('#events-listing-blocks-template').html() );

                    if (response.blocks[i].image) {
                        $content.find('.block--media a')
                            .attr('data-img', response.blocks[i].image);
                    }
                    else {
                        $content.addClass('block--item--noimage');
                        $content.find('.block--media').remove();
                    }

                    $content.find('a').attr('href', response.blocks[i].url);
                    $content.find('.block--category').text(response.blocks[i].category);
                    $content.find('.block--title').html(response.blocks[i].title);
                    $content.find('.block--datevenue').text(response.blocks[i].dateVenue);

                    $('.events-listing-blocks .block--error').before($content);
                }

                FAMSF.blocks.createImages();
                if (/^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
                    FAMSF.eventsListing.computeBlockHeight();
                }
                FAMSF.calendar.updateWaypoints();
            }
            else {
                $('.events-listing-blocks .block--item').remove();
                $('.events-listing-blocks .block--error').addClass('visible');

                FAMSF.calendar.resetCalendar();
            }

            FAMSF.transition.refresh();
        },

        computeBlockHeight: function() {
            var $blocks = $('.events-listing-blocks .block--item');

            $blocks.removeAttr('style');
            $blocks.each(function(i) {
                $(this).height( $(this).height() );
            });
        }
    };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.figures = {
    init: function () {
      var prevBreakpoint;
      var slider;

      FAMSF.eventHandler.subscribe('EVENT_RESIZE', function () {
        var currentBreakpoint = FAMSF.utils.getBreakpoint();
        
        if (currentBreakpoint !== prevBreakpoint) {
          if (currentBreakpoint === 'small') {
            slider = new FAMSF.Slider($('.figures .slider'), {peeking: false});
          }
          else if (slider) {
            slider.destroy();
          }
        } 

        prevBreakpoint = currentBreakpoint;
      });
      
      $(window).resize();
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.header = {
    init: function () {
      var self = this;

      this.$header      = $('.header');
      this.$headerTall  = $('.header--tall');
      this.$headerShort = $('.header--short');
      this.$navigation  = $('.navigation');

      // Search modal
      self.$header.find('.header__search-button').magnificPopup({
        alignTop: true,
        closeOnBgClick: false,
        items: {
          src:  '#header__search',
          type: 'inline'
        }
      });

      // Watermark check and toggle
      self.hideWatermark();

      // Header navigation toggle
      self.$header.on('click', '.header__nav-toggle', function(e) {
        var $this   = $(this);
        var $target = $($this.data('target'));
        
        if (!self.$navigation.hasClass('navigation--expanded')) {
          $this.addClass('header__nav-toggle--active');
          self.expandNavigation($target);
        } else {
          $this.removeClass('header__nav-toggle--active');
          self.retractNavigation();
        }
      });

      // Header navigation items
      self.$header.on('click', '.header__nav-item a', function(e) {
        var $this   = $(this);
        var $target = $($this.data('target'));

        if ($target.length) {
          e.preventDefault();

          if (!$target.hasClass('navigation__nav--active')) {
            $this
              .closest('.header__nav-item')
              .addClass('header__nav-item--active')
              .siblings('.header__nav-item--active')
              .removeClass('header__nav-item--active');

            self.expandNavigation($target);
          }
          else {
            $this
              .closest('.header__nav-item')
              .removeClass('header__nav-item--active');

            self.retractNavigation();
          }
        }
      });

      // Navigation links
      self.$navigation.on('click', '.navigation__nav-item a, .navigation__back-button', function(e) {
        var $this   = $(this);
        var $target = $($this.data('target'));

        if ($target.length) {
          e.preventDefault();

          $this
            .closest('.navigation__nav')
            .removeClass('navigation__nav--active');

          $target.addClass('navigation__nav--active');
        }
      });

      // Navigation close button
      self.$navigation.on('click', '.navigation__close-button', function(e) {
        self.retractNavigation();
      });

      // Dropdown toggle
      self.$navigation.on('click', '.navigation__dropdown-toggle', function(e) {
        $(this)
          .toggleClass('navigation__dropdown-toggle--active')
          .next('.navigation__dropdown')
          .slideToggle();
      });

      // Expand/retract short header
      if (self.$headerTall.length) {
        $(window).on('scroll', function() {
          if (!self.$navigation.hasClass('navigation--expanded')) {
            var headerOffset    = self.$headerTall.offset(),
                waypoint        = headerOffset.top + self.$headerTall.innerHeight(),
                windowScrolltop = $(window).scrollTop();

            self.$headerShort.toggleClass('header--expanded', windowScrolltop > waypoint);
            self.$navigation.find('.navigation__logo-container').toggleClass('is-hidden', windowScrolltop > waypoint);
          }
        }).trigger('scroll');
      }

      // Retract navigation on resize
      var prevBreakpoint = FAMSF.utils.getBreakpoint();
      $(window).on('resize', function() {
        var currentBreakpoint = FAMSF.utils.getBreakpoint();

        if (self.$navigation.hasClass('navigation--expanded')) {
          if (currentBreakpoint !== prevBreakpoint) {
            self.retractNavigation();
          }

          $('body').css('width', 'auto');
        }

        prevBreakpoint = currentBreakpoint;
      });
    },
    hideWatermark: function() {
      var self = this;

      if (self.$header.hasClass('header--expanded')) {
        self.$navigation.find('.navigation__logo-container').addClass('is-hidden');
      }
    },
    expandNavigation: function($target) {
      var self   = this;
      var navTop = this.$headerTall.innerHeight() - $(window).scrollTop() - 1;
      var $body  = $('body');

      if (self.$headerShort.hasClass('header--expanded') || self.$headerTall.is(':hidden')) {
        navTop = self.$headerShort.innerHeight() - 1;
      }

      if (self.$navigation.hasClass('navigation--expanded')) {
        $target
          .addClass('navigation__nav--active')
          .siblings('.navigation__nav--active')
          .removeClass('navigation__nav--active');
      }

      self.$navigation
        .removeClass('navigation--scrollable')
        .addClass('navigation--expanded')
        .css('top', navTop)
        .one('transitionend', function(e) {
          self.$navigation.addClass('navigation--scrollable');
          $target.addClass('navigation__nav--active');
        });

      var bodyWidth = $body.innerWidth();

      $body
        .css('width', bodyWidth)
        .addClass('no-scroll');
    },
    retractNavigation: function() {
      var self = this;

      self.$header
        .find('.header__nav-toggle--active, .header__nav-item--active')
        .removeClass('header__nav-toggle--active header__nav-item--active');

      self.$navigation
        .removeClass('navigation--scrollable')
        .removeClass('navigation--expanded')
        .one('transitionend', function(e) {
          self.$navigation
            .find('.navigation__nav--active')
            .removeClass('navigation__nav--active');

            $('body')
              .css('width', 'auto')
              .removeClass('no-scroll');
        });
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.joinGiveIntro = {
    $header: $('.header'),
    $intro: $('.join-give-intro'),
    $introMain: $('.join-give-intro__main'),
    $sidebar: $('.join-give-intro__aux'),
    sidebarHeight: 0,
    introHeight: 0,
    introMainHeight: 0,
    scrollPosition: 0,
    sidebarPosition: 0,
    scrollAnimOffset: 100, // number of pixels a .transition element travels when fading/scrolling in
    sidebarFixedTop: 105, // must have the same top value in css .join-give-intro__aux.fixed per breakpoint

    initSidebar: function() {
      var self = FAMSF.joinGiveIntro;

      self.sidebarHeight = self.$sidebar.outerHeight();
      self.introHeight = self.$intro.outerHeight();
      self.introMainHeight = self.$introMain.outerHeight();
      self.scrollPosition = $(window).scrollTop();

      if (self.$intro.hasClass('transition--completed')) {
        self.sidebarPosition = self.$intro.offset().top;
      } else {
        self.sidebarPosition = self.$intro.offset().top - self.scrollAnimOffset;
      }
    },

    dockSidebar: function() {
      var self = FAMSF.joinGiveIntro;

      if (self.$intro.hasClass('transition--completed')) {
        self.sidebarPosition = self.$intro.offset().top;
      } else {
        self.sidebarPosition = self.$intro.offset().top - self.scrollAnimOffset;
      }

      // update sidebarFixedTop value per breakpoint
      if (FAMSF.utils.getBreakpoint() === 'medium') {
        self.sidebarFixedTop = 140; // value is from .join-give-intro__aux.fixed { top: value }
      }

      if (FAMSF.utils.getBreakpoint() === 'large' || FAMSF.utils.getBreakpoint() === 'xlarge') {
        self.sidebarFixedTop = 105; // value is from .join-give-intro__aux.fixed { top: value }
      }


      if (self.introMainHeight <= self.sidebarHeight) {
        return; // if right side is taller than left side, no need to dock
      }

      if (self.$intro.hasClass('transition--completed')) {
        self.scrollPosition = $(window).scrollTop();

        if (self.scrollPosition > self.sidebarPosition - self.sidebarFixedTop) {
          // define where we want the sidebar to start scrolling out of view
          var targetPos = self.sidebarPosition + self.introHeight - self.sidebarHeight - self.sidebarFixedTop;

          if (self.scrollPosition > targetPos) {
            self.$sidebar.removeClass('fixed').addClass('scrolled');

          } else {
            self.$sidebar.removeClass('scrolled').addClass('fixed');
          }
        } else {
          self.$sidebar.removeClass('fixed scrolled');
        }
      }
    },

    init: function() {
      var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);

      if (navigator.userAgent.match(/(iPod|iPhone|iPad)/) ) {
          $('.join-give-intro__aux').addClass('ios-device');
          return false;
      }

      if (FAMSF.utils.getBreakpoint != 'small') {
        var self = FAMSF.joinGiveIntro;

        // timeouts are to let .transition behaviors complete so we don't get incorrect offset() values
        window.setTimeout(self.initSidebar, 200);
        window.setTimeout(self.dockSidebar, 200);

        FAMSF.eventHandler.subscribe('EVENT_RESIZE', function () {
          self.initSidebar();
          self.dockSidebar();
        });

        FAMSF.eventHandler.subscribe('EVENT_SCROLL_INSTANT', function () {
          self.dockSidebar();
        });
      }
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.manifesto = {
    animateUnderlines: function() {
      var manifestoScrollPos = $('.manifesto--deyoung').offset().top + 200,
        windowScrollPos = $(window).scrollTop() + $(window).height();

      if (windowScrollPos > manifestoScrollPos) {
        $('.manifesto a').each(function() {
          var $this = $(this),
              delayTime = (Math.floor(((Math.random() * 80) + 1)) * 10);

          window.setTimeout(function(){
            $this.addClass('is-underlined');
          }, delayTime);
        });
      }
    },

    initManifestoImages: function() {
      $('.manifesto__image').each(function() {
        var $this = $(this),
            thisImg = $this.find('img').attr('src');

        $this.css('background-image', 'url("' + thisImg + '")');
      });
    },

    displayManifestoImage: function(el) {
      var imgCounter = el.data('counter'),
          $linkedImg = el.closest('.manifesto').find('.manifesto__image[data-counter=' + imgCounter + ']');

      if ($linkedImg.hasClass('is-visible')) {
        $linkedImg.removeClass('is-visible');
      } else {
        $linkedImg.addClass('is-visible');
      }
    },

    init: function () {
      FAMSF.manifesto.animateUnderlines();
      FAMSF.manifesto.initManifestoImages();

      FAMSF.eventHandler.subscribe('EVENT_SCROLL', function () {
        FAMSF.manifesto.animateUnderlines();
      });

      $('.manifesto a').hover(function() {
        FAMSF.manifesto.displayManifestoImage($(this));
      });
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.map = {
    init: function () {

      var _map = $('.js-map');
      var map = {
        $pin : $('.js-map-pin', _map),
        $media : $('.js-map-media', _map)
      };


      // events
      map.$pin.on('mouseover', function(e){
        var $this = $(this);
        map.$media.css({
          'background-image': 'url(' + $this.data('bgImg') +')',
          'opacity' : '.5'
        });
      });

      map.$pin.on('mouseout', function(e){
        map.$media.css('opacity', '0');
      });
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.mediaGallery = {
    init: function () {
      var $mediaGallery      = $('.media-gallery');
      var $mediaGalleryItems = $('.media-gallery__items', $mediaGallery);
      var mediaGalleryOptions = {
        peeking: false
      };

      new FAMSF.Slider($mediaGalleryItems, mediaGalleryOptions);
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.mediaOverlay = {
    init: function () {
      var mo = FAMSF.mediaOverlay,
          $mediaOverlayItems,
          $dataAttributes = $('.data-attributes'),
          svgSpritePath = $dataAttributes.data('svgSpritePath'),
          carouselArrowLeft = svgSpritePath + '#' + $dataAttributes.data('carouselArrowLeft'),
          carouselArrowRight = svgSpritePath + '#' + $dataAttributes.data('carouselArrowRight'),
          overlayClose = svgSpritePath + '#' + $dataAttributes.data('overlayClose');

      if ($('main').is('.page--regular-exhibitions, .page--special-exhibitions')) {
        $mediaOverlayItems = $('.media-overlay__item');
      } else {
        $mediaOverlayItems = $('.media-overlay__item.video');
      }

      if ($mediaOverlayItems.length) {
        $mediaOverlayItems.on('click', function(e) {
          e.preventDefault();

          var $this = $(this),
              $mediaItems = $this.hasClass('media-overlay__item--gallery') ? $mediaOverlayItems.filter('.media-overlay__item--gallery') : $this,
              mediaItems  = $.map($mediaItems, function(n) {
                var data = $(n).data();
                data.type = data.type === 'video' ? 'iframe' : data.type;

                return data;
              });

          $.magnificPopup.open({
            alignTop: true,
            closeOnBgClick: false,
            closeMarkup: '<button title="Close button" type="button" class="mfp-close">%title%</button>',
            gallery: {
              enabled: true,
              arrowMarkup: '<button title="%dir% button" type="button" class="mfp-arrow mfp-arrow-%dir%">%title%</button>',
              tPrev: '<svg role="img"><use xlink:href="' + carouselArrowLeft + '"></use></svg>',
              tNext: '<svg role="img"><use xlink:href="' + carouselArrowRight + '"></use></svg>',
              tCounter: '%curr% / %total%',
              markup: '<div></div>'
            },
            items: mediaItems,
            mainClass: 'media-overlay',
            tClose: '<svg role="img"><use xlink:href="' + overlayClose + '"></use></svg>',
            type: 'image',
            image: {
              markup: '<div class="mfp-figure">'+
              '<div class="mfp-close"></div>'+
              '<figure>'+
              '<div class="mfp-img"></div>'+
              '<figcaption>'+
              '<div class="mfp-bottom-bar">'+
              '<div class="mfp-title"></div>'+
              '</div>'+
              '</figcaption>'+
              '</figure>'+
              '<div class="mfp-counter"></div>'+
              '</div>'
            },
            iframe: {
              markup: '<div class="mfp-iframe-container">'+
              '<div class="mfp-close"></div>'+
              '<div class="mfp-iframe-scaler">'+
              '<iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe>'+
              '</div>'+
              '<div class="mfp-title"></div>'+
              '<div class="mfp-counter"></div>'+
              '</div>',
              patterns: {
                youtube: {
                  index: 'youtube.com',
                  id: 'v=',
                  src: '//www.youtube.com/embed/%id%?autoplay=0'
                },
                vimeo: {
                  index: 'vimeo.com/',
                  id: '/',
                  src: '//player.vimeo.com/video/%id%?autoplay=0'
                }
              }
            },
            callbacks: {
              open: function(){
                $('html,body').css({
                  'overflow': 'hidden',
                  'position': 'relative'
                });
              },
              close: function(){
                $('html,body').removeAttr('style');
              },
              change: function(){
                //mo.mediaResize(this);
              },
              imageLoadComplete: function(){
                //mo.mediaResize(this);
              },
              updateStatus: function(data) {
                mo.mediaResize(this);
              },
              resize: function(data) {
                mo.mediaResize(this);
              }

            }
          }, $mediaItems.index($this));
        });

      }
    },
    mediaResize: function( mp ){
      var interval, $iframe, iframe = {};
      var $container      = mp.container; // mfp-container
      var $contentCont    = mp.contentContainer; // mfp-content
      var $content        = mp.content; // mfp-figure
      var $img            = $('img', $container); // mfp-img

      var newHeight       = function( objOffsetTop, captionHeight ) { return $container.height() - (objOffsetTop - $(document).scrollTop()) - captionHeight - 50; };



      clearInterval(interval);

      // for img
      if ( $img.length ) {
        $img.css({
          maxHeight : newHeight( $img.offset().top, $('.mfp-title', $container).outerHeight() ),
          opacity   : 1
        });
      }

      // for iframe
      if ( mp.currItem.type === "iframe" ) {
        interval = setInterval(function(){
          if ( $('iframe', $container).length ) {

            $iframe = $('.mfp-iframe-scaler', $container);
            iframe.H = newHeight( $iframe.offset().top, $('.mfp-title', $container).outerHeight() );
            iframe.W = 16/9*iframe.H;

            if ( iframe.W > $contentCont.width() ){
              iframe.W = $contentCont.width();
              iframe.H = 9/16*iframe.W;
            }

            $iframe.css({
              height: iframe.H,
              width: iframe.W,
              padding: 0,
              margin: '0 auto'
            });

            clearInterval(interval);
          }
        }, 200);

      }
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.membershipLevel = {
    init: function () {
      var ML = this;
      var $context = $('.js-membership-level');
      var $btn = $('.js-show-btn', $context);


      $btn.on('click', function(e){
        e.preventDefault();
        var $this     = $(this),
            $item     = $this.parents('.js-item'),
            $content  = $('.js-item-content', $item);

        if ( $this.hasClass('is-expanded')) {
          $this
            .removeClass('is-expanded');

          $content
            .css('max-height', $content.attr('data-min-height') + 'px')
            .removeClass('is-expanded');

          $("html, body")
            .animate({ scrollTop: $item.offset().top - $('header').outerHeight() }, 1000);
        } else {
          $this
            .addClass('is-expanded');

          $content
            .css('max-height', $('.js-item-content-inner', $content).outerHeight())
            .addClass('is-expanded');

        }
        ML.setBtnLabel();

        // disable on click, enabled when content complete to expanded/collapsed
        $this.attr('disabled', true);
        setTimeout(function(){
          $this.attr('disabled', false);
        }, 1000);
      });

      ML.tabHandler();
      ML.contentHeightHandler();
      ML.urlFragmentHandler();
      ML.resizeHandler();
    },

    setBtnLabel: function() {
      var $context = $('.js-membership-level');
      var $btn = $('.js-show-btn', $context);

      var _label = {
        expanded  : 'Show less benefits -',
        collapsed : 'Show more benefits +',
        mobile : {
          expanded  : 'Hide benefits -',
          collapsed : 'Show benefits +',
        }
      };

      if ( FAMSF.utils.getBreakpoint() === "small" ){
        $btn.not('.is-expanded').text(_label.mobile.collapsed);
        $btn.filter('.is-expanded').text(_label.mobile.expanded);
      } else {
        $btn.not('.is-expanded').text(_label.collapsed);
        $btn.filter('.is-expanded').text(_label.expanded);
      }
    },
    
    tabHandler: function(){
      var ML = this;
      var $context = $('.js-membership-level');
      var $btn = $('.js-tab-btn', $context);

      $btn.on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        
        $btn
          .removeClass('is-active');

        $this
          .addClass('is-active');

        window.location.hash = $this.attr('href');
        ML.urlFragmentHandler();
      });
    },
    urlFragmentHandler: function(){
      var hashValue = window.location.hash.substr(1),
          $context  = $('.js-membership-level'),
          $tabBtn   = $('.js-tab-btn', $context);
          $tabItem  = $('.js-tab-item', $context);

      if (hashValue) {
        $tabBtn.removeClass('is-active');
        $tabItem.removeClass('is-active');

        $tabBtn.filter('[href="#' + hashValue + '"]').addClass('is-active');
        $tabItem.filter('[data-tab-id="' + hashValue + '"]').addClass('is-active');

        $('.media-bg', $context).lazyLoadXT();
      }
    },
    contentHeightHandler : function(){
      var $context  = $('.js-membership-level'),
          $item     = $('.js-item', $context);

      
      $.each($item, function(){
        var $this = $(this), 
            $content  = $('.js-item-content', $this),
            $list = $('ul', $content),
            $listItem = $('li', $content),
            $btn = $('.js-show-btn', $this),
            minHeight = 0,
            maxHeight = $('.js-item-content-inner', $content).outerHeight(),
            maxItem = 2;

        if ( $listItem.length > maxItem ) { 
          minHeight = $listItem.filter(':nth-child('+ (maxItem+1) +')').position().top;
        } else {
          $btn.hide();
        }
          
        if ( FAMSF.utils.getBreakpoint() === "small" ){
          $btn
            .removeAttr('style');
          minHeight = 0;
        } else {
          if ( $listItem.length <= maxItem ) {
            minHeight = 'none';
          }
        }

        $content
          .attr('data-min-height', minHeight);

        if ( !$btn.hasClass('is-expanded')){
          $content
            .css('max-height', minHeight);
        } else {
          $content
            .css('max-height', maxHeight);
        }
      });

    },
    resizeHandler: function() {
      var ML = this;

      FAMSF.eventHandler.subscribe('EVENT_RESIZE_INSTANT', function(){
        ML.setBtnLabel();
        ML.contentHeightHandler();
      });
    }

  };
})(jQuery);
FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.newsletter = {
    init: function () {
      var module = FAMSF.newsletter;

      var $context = $('.js-newsletter');
      var $form = $('form', $context);
      var $email = $('input[name="email"]', $context);
      var _msg = {
        tmpl : '<span class="textfield__msg">_msg_</span>',
        invalid  : "Invalid email address.",
        timeout : "Something went wrong. Try again.",
        success : "Thanks for signing up."
      };



      $email.on('keyup', function(){
        if ( $email.val().length ) { $email.parent().addClass('is-filled'); }
        else {
          $email.parent().removeClass('is-filled');
        }
      }).on('focus', function(){
        $email.parent().addClass('is-focus');
      }).on('blur', function(){
        $email.parent().removeClass('is-focus');
      });

      $form.on('submit', function(event){
        event.preventDefault();
        // Serialize the form data.
        var formData = $form.serialize();

        $('.textfield__msg', $context).remove();

        // validate email format
        if ( !/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm.test( $email.val() ) ){
          if ( !$email.next('.textfield__msg').length ){
            $(_msg.tmpl).insertAfter($email);
          }
          $email.next().text(_msg.invalid);
          return;
        }

        if ( !$email.next('.textfield__progress').length ){
          $('<div class="textfield__progress"><span class="textfield__progress-bar"></span></div>').insertAfter($email);
        }

        $email.attr('disabled', true);


        var _progress = {
          increment : 0,
          interval : 25,
          max: 800
        };
        var progress = function(){
          _progress.increment += _progress.interval;
          var width = _progress.increment / _progress.max *100;

          $('.textfield__progress-bar', $context).width( width.toFixed(2) + '%' );
          if ( _progress.increment == _progress.max ){
            clearInterval(interval);
            $('.textfield__progress', $context).remove();
            $(_msg.tmpl).insertAfter($email);
            $email.next().text(_msg.success);
            $email.removeAttr('disabled');
          }
        };
        var interval = setInterval(progress, _progress.interval);
      });
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.permanentCollections = {
    init: function () {
      var $context = $('.js-permanent-collections');


      $.each($context, function(){
        var $this = $(this);
        var $collections = $('a[data-img]', $this);

        $('.collection__hero-link', $this).attr('disabled', true);
        
        var setCollection = function( $elem ){
          $('.collection__hero .media-bg', $this).css('background-image', 'url(' + $elem.data('img') +')');
          $('.collection__hero-link', $this).attr('href', $elem.attr('href'));
          $('.collection__hero-link', $this).attr('disabled', false);

          $collections.removeClass('is-active');
          $elem.addClass('is-active');
        };
        
        // events
        $collections.on('mouseover', function(e){
          var $collection = $(this);
          setCollection($collection);
        }); 
      });
    }
  };
})(jQuery);
FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.ticker = {
    init: function () {
      setTimeout(function() {
        var $ticker = $('.ticker');
        var $tickerInner   = $('.ticker__inner', $ticker);
        var $tickerTime    = $('.ticker__time', $ticker);
        var offsetLeft     = $ticker.width() - $tickerTime.width();
        var timeMultiplier = 20;

        $ticker.addClass('ticker--animating');
        $tickerInner
          .css('left', offsetLeft)
          .animate({
            left: -$tickerInner.width()
          }, {
            duration: $tickerInner.width() * timeMultiplier,
            easing: 'linear',
            complete: function() {
              offsetLeft = $ticker.width() - $tickerTime.width();

              $tickerInner
                .css('left', '100%')
                .animate({
                  left: offsetLeft
                }, {
                  duration: $tickerTime.width() * timeMultiplier,
                  easing: 'linear',
                  complete: function() {
                    $ticker.removeClass('ticker--animating');
                    $tickerInner.css('left', 'auto');
                  }
                });
            }
          });
      }, 3000);
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.visitNotification = {
    init: function () {
      new FAMSF.Slider($('.visit-notification .slider'), {peeking: false});
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
    FAMSF.blocks = {
        init: function () {
            var Blocks = FAMSF.blocks;
            var _block = $('.close-block');
            _block.each(function () {
                $(this).on('click', function () {
                    if ($(this).is('.open')) {
                        $(this).removeClass('open');
                        $(this).parents('.block--text').removeClass('open');
                        $(this).siblings('.block--context').removeClass('open').slideUp(200);
                    } else {
                        $('.close-block.open').click();
                        $(this).addClass('open');
                        $(this).parents('.block--text').addClass('open');
                        $(this).siblings('.block--context').addClass('open').slideDown(200);
                    }
                });
            });
            Blocks.createImages();
            Blocks.loopImages();
            Blocks.enableLooping();
            FAMSF.eventHandler.subscribe('EVENT_RESIZE_INSTANT', Blocks.enableLooping);
        },
        createImages: function() {
            var $context = $('.js-blocks');
            var $block = $('.block--item', $context);

            $.each($block, function(){
                var $this = $(this);
                var $content = $('.content', $this);

                if ($content.length > 0) {
                  var images = $content.data('img').split(/,/).map(function(val){
                      if (val !== "") {
                          $('<span data-bg="'+ val +'" />').appendTo($content);
                      }
                  });

                  $('span:first', $content).addClass('is-active');
                  $('span:first', $content).lazyLoadXT();
                }
            });
        },
        loopImages: function() {
            var Blocks = this;
            var interval;
            var $context = $('.js-blocks');
            var $block = $('.block--item', $context);
            var $speed = 700;

            // events
            $block.on('mousehover mouseenter', function(){
                var i, bgImg,
                    $this = $(this),
                    $img = $('span', $this),
                    $firstImg = $img.first(),
                    $nextImg = $img.next(),
                    $imgCount = $img.length;

                if ( !$context.hasClass('loopImages') || $img.length === 1 ){
                    return;
                }

                clearInterval(interval);
                $img
                    .removeClass('is-active');

                $firstImg
                    .addClass('is-active');

                Blocks.imageLoadChecker($firstImg);

                $nextImg.css('left', 0);
                $img.lazyLoadXT();

                interval = setInterval(function(){
                    var $activeImg = $img.filter('.is-active:first'), src;
                    $nextImg = $activeImg.next().length ? $activeImg.next() : $img.first();

                    $(window).trigger('scroll');

                    //check next active image if fully loaded then add class .img-loaded
                    Blocks.imageLoadChecker($nextImg);

                    if ( !$nextImg.hasClass('img-loaded') ){
                        return;
                    }

                    $nextImg
                        .addClass('is-active')
                        .css('left', '');

                    $activeImg
                        .removeClass('is-active');

                    if ($nextImg.next().length){
                        $nextImg.next()
                            .css('left', 0);
                    }


                }, $speed);

            });

            $block.on('mouseleave', function(){
                var $this = $(this),
                    $img = $('span', $this);

                clearInterval(interval);
                $img
                    .removeClass('is-active')
                    .css('left', '');
                $img.first().addClass('is-active');

            });
        },
        enableLooping: function() {
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $('.js-blocks').removeClass('loopImages');
                return false;
            }
            if ( FAMSF.utils.getBreakpoint() === "small" || FAMSF.utils.getBreakpoint() === "medium" ){

                $('.js-blocks').removeClass('loopImages');
            } else {
                $('.js-blocks').addClass('loopImages');
            }
        },
        imageLoadChecker: function(img){
            if ( img.css('background-image') !== '' || !img.hasClass('img-loaded') ){
                var src = img.css('background-image').replace(/(^url\()|(\)$|[\"\'])/g, '');
                $('<img/>').attr('src', src).on('load', function() {
                    $(this).remove();
                    img.addClass('img-loaded');
                });
            }
        }
    };
})(jQuery);

FAMSF = window.FAMSF || {};

FAMSF.Slider = function($el, options) {
  this.$el = $el;

  var $dataAttributes = $('.data-attributes'),
      svgSpritePath = $dataAttributes.data('svgSpritePath'),
      carouselArrowLeft = svgSpritePath + '#' + $dataAttributes.data('carouselArrowLeft'),
      carouselArrowRight = svgSpritePath + '#' + $dataAttributes.data('carouselArrowRight');

  var defaults = {
    autoplay: true,
    autoplaySpeed: 500000,
    dots: true,
    infinite: true,
    peeking: true,
    slidesToShow: 1,
    prevArrow: "<a href='#' class='slick-prev'><svg role='img'><use xlink:href='" + carouselArrowLeft + "'></use></svg></a>",
    nextArrow: "<a href='#' class='slick-next'><svg role='img'><use xlink:href='" + carouselArrowRight + "'></use></svg></a>",
    customPaging: function(slider, i) {
      return '<span class="counter">' + (i + 1) + '/' + slider.slideCount + '</span>';
    }
  };

  var settings = $.extend({}, defaults, options);

  if (settings.peeking) {
    $el.on('mouseenter', '.slick-arrow', function() {
      var $this    = $(this);
      var $current = $('.slick-current', $el);

      if ($this.hasClass('slick-next')) {
        $current
          .next('.slick-slide')
          .addClass('slick-slide--peeking slick-slide--peeking-next');
      }
      else {
        $current
          .prev('.slick-slide')
          .addClass('slick-slide--peeking slick-slide--peeking-prev');
      }
    });

    $el.on('mouseleave click', '.slick-arrow', function() {
      $('.slick-slide--peeking', $el)
        .removeClass('slick-slide--peeking-next slick-slide--peeking-prev')
        .one('transitionend', function() {
          $(this).removeClass('slick-slide--peeking');
        });
    });

    $el.on('afterChange', function (event, slick, currentSlide) {

      var $current = $('.slick-current', $el);

      $current
        .next('.slick-slide')
        .find('.media-bg')
        .lazyLoadXT({show: true});

      $current
        .prev('.slick-slide')
        .find('.media-bg')
        .lazyLoadXT({show: true});



      var $activeArrow = $('.slick-arrow', $el).filter(function() { return $(this).is(":hover"); });

      if ( $activeArrow.length ) {
        $activeArrow.trigger('mouseenter');
      }
    });

    $el.on('init', function (event, slick, currentSlide) {
      $('.slick-cloned', $el).each(function() {
        var $this  = $(this);
        var $image = $this.find('.media-bg');
        var src    = $image.data('bg');

        if (src) {
          $image.css('background-image', 'url("' + src + '")');
        }
      });
    });
  }

  $el.on('afterChange', function (event, slick, currentSlide) {
      $(window).trigger('scroll');
  });

  $el.on('beforeChange', function (event, slick, currentSlide) {
      console.log('before');

      var $current = $('.slick-current', $el);

      $current
        .prev('.slick-cloned')
        .css('background-image', 'url("' + $current.prev('.slick-cloned').data('bg') + '")')
        .removeAttr('data-bg');
      
      $current
        .next('.slick-cloned')
        .css('background-image', 'url("' + $current.next('.slick-cloned').data('bg') + '")')
        .removeAttr('data-bg');
  });

  $el.slick(settings);
};

FAMSF.Slider.prototype.destroy = function() {
  this.$el.slick('unslick');
};

FAMSF = window.FAMSF || {};

(function($) {
  var dragIcon = {};
  var contain, xp, yp, wrapper;
  var mousedown = false;

  FAMSF.drag = {
    init: function() {
      new Dragdealer('horizontal-scroll', {
        horizontal: true,
        vertical: false,
        slide: true,
        loose: true
      });

      dragIcon.el = $('.grab-hover__follow');
      dragIcon.isActive = false;
      dragIcon.wasSeen = false;

      contain = $('.related-events');
      wrapper = $('.handle');
      xp = 0;
      yp = 0;

      FAMSF.drag.addMouseListeners();
    },

    removeDragIcon: function(){
      dragIcon.wasSeen = true;
      dragIcon.el.removeClass('active');
    },

    addMouseListeners: function() {
      var mousedown = false;
      var dragging = false;

      contain.on('mousedown', function(e) {

      if( dragIcon.isActive ){
        FAMSF.drag.removeDragIcon();
      }

      // if right mouse button
      if(e.button || e.ctrlKey){
        return;
      }

      mousedown = true;
      e.preventDefault();
    });

    $(window).on('mousemove', function(e) {
      if (dragIcon.isActive) {
          xp = e.clientX;
          yp = e.clientY;
      }

      dragIcon.el.css({left: xp, top: yp});

      if (!mousedown) { return; }

      dragging = true;
      mousedown = true;
    });

    wrapper.on('click', function(e){
      if (dragging) {
          e.preventDefault();
          e.stopPropagation();
      }

      FAMSF.drag.removeDragIcon();
    });

    wrapper.on('mouseenter', function(e){
      if (dragIcon.wasSeen) { return; }

      dragIcon.isActive = true;
      dragIcon.el.addClass('active');
    });

    wrapper.on('mouseleave', function(e){
      dragIcon.el.removeClass('active');
      dragIcon.isActive = false;
      });
    }
  };
})(jQuery);

// BEGIN MODULE: EVENT HANDLER

FAMSF = window.FAMSF || {};

(function($) {
  FAMSF.eventsHandler = {
    init: function () {
      FAMSF.eventHandler = new SubPub();

      FAMSF.resizeHandler = function () {
        // SUB/PUB RESIZE
        var resizeObject = {};
        FAMSF.$window.on('resize', function () {
          resizeObject = {
            winWidth:   Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
            winHeight:  Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
          };

          FAMSF.eventHandler.publish('EVENT_RESIZE_INSTANT', resizeObject);
        });

        FAMSF.$window.on('resize', FAMSF.utils.debounce(function () {
          FAMSF.eventHandler.publish('EVENT_RESIZE', resizeObject);
        }, 200));
      };

      FAMSF.scrollHandler = function () {
        // SUB/PUB SCROLL
        var resizeObject = {};
        FAMSF.$window.on('scroll', function () {
          scrollObject = {
            scrollY:  Math.max(window.scrollY || document.documentElement.scrollTop),
            scrollX:  Math.max(window.scrollX || document.documentElement.scrollLeft)
          };

          FAMSF.eventHandler.publish('EVENT_SCROLL_INSTANT', scrollObject);
        });

        FAMSF.$window.on('scroll', FAMSF.utils.debounce(function () {
          FAMSF.eventHandler.publish('EVENT_SCROLL', scrollObject);
        }, 100));
      };

      // initialize each event handler
      FAMSF.resizeHandler();
      FAMSF.scrollHandler();
    }
  };
})(jQuery);

// END MODULE: EVENT HANDLER

// BEGIN MODULE: LAZY LOADING

FAMSF = window.FAMSF || {};

(function($) {
  FAMSF.lazyLoad = {
    init: function() {
      if ( !$('[data-src]').length ) { return; }

      $(document).find('[data-src]').lazyLoadXT();
    }
  };
})(jQuery);

// END MODULE: LAZY LOADING
(function(Modernizr){

  // Here are all the values we will test. If you want to use just one or two, comment out the lines of test you don't need.
  var tests = [
    //{ name: 'svg', value: 'url(#test)' }, // False positive in IE, supports SVG clip-path, but not on HTML element
    //{ name: 'inset', value: 'inset(10px 20px 30px 40px)' },
    //{ name: 'circle', value: 'circle(60px at center)' },
    //{ name: 'ellipse', value: 'ellipse(50% 50% at 50% 50%)' },
    { name: 'polygon', value: 'polygon(50% 0%, 0% 100%, 100% 100%)' }
  ];

  var t = 0,
      name, value, prop,
      testClipPath = function() {
        // Try using window.CSS.supports
        if ( 'CSS' in window && 'supports' in window.CSS ) {
          for (var i = 0; i < Modernizr._prefixes.length; i++) {
            prop = Modernizr._prefixes[i] + 'clip-path';

            if ( window.CSS.supports(prop,value) ) { return true; }
          }
          return false;
        }
        // Otherwise, use Modernizr.testStyles and examine the property manually
        return Modernizr.testStyles('#modernizr { '+Modernizr._prefixes.join('clip-path:'+value+'; ')+' }',function(elem, rule) {
          var style = getComputedStyle(elem),
              clip = style.clipPath;

          if ( !clip || clip == "none" ) {
            clip = false;

            for (var i = 0; i < Modernizr._domPrefixes.length; i++) {
              test = Modernizr._domPrefixes[i] + 'ClipPath';
              if ( style[test] && style[test] !== "none" ) {
                clip = true;
                break;
              }
            }
          }

          return Modernizr.testProp('clipPath') && clip;
        });
      };

  for (; t < tests.length; t++) {
    name = tests[t].name;
    value = tests[t].value;
    Modernizr.addTest('cssclippath' + name, testClipPath());
  }
})(Modernizr);

(function(){
  var isTouch = false;
  var isTouchTimer; 
  var curRootClass = '';
   
  function addtouchclass(e){
    clearTimeout(isTouchTimer);

    isTouch = true;

    if (curRootClass != 'can-touch') {
      curRootClass = 'can-touch';
      document.documentElement.classList.add(curRootClass);
    }

    isTouchTimer = setTimeout(function(){isTouch = false;}, 500);
  }
   
  function removetouchclass(e){
      if (!isTouch && curRootClass == 'can-touch'){
        isTouch = false;
        curRootClass = '';
        document.documentElement.classList.remove('can-touch');
      }
  }
   
  document.addEventListener('touchstart', addtouchclass, false);
  document.addEventListener('mouseover', removetouchclass, false);
})();

FAMSF = window.FAMSF || {};

(function($) {
  FAMSF.transition = {
    init: function() {
      $('.transition').waypoint(function(direction) {
        $(this.element).addClass('transition--completed');
          this.destroy(); // unbind
      }, {
        offset: '102%'
      });
    },
    refresh: function(){
      Waypoint.refreshAll();
      console.log('reset');
    }
  };
})(jQuery);

FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.utils = {
    getBreakpoint: function() {
      var breakpoint = 'small';

      if (window.matchMedia('(min-width: 1401px)').matches) {
        breakpoint = 'xlarge';
      }
      else if (window.matchMedia('(min-width: 1001px)').matches) {
        breakpoint = 'large';
      }
      else if (window.matchMedia('(min-width: 721px)').matches) {
        breakpoint = 'medium';
      }

      return breakpoint;
    },

    debounce: function(func, wait, immediate) {
      var timeout;
      return function () {
        var context = this, args = arguments;
        var later = function() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    }
  };
})(jQuery);
