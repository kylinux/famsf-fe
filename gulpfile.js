'use strict';

var argv         = require('yargs').argv,
    autoprefixer = require('gulp-autoprefixer'),
    browserSync  = require('browser-sync').create(),
    cheerio      = require('gulp-cheerio'),
    concat       = require('gulp-concat'),
    del          = require('del'),
    fs           = require('fs'),
    gulp         = require('gulp'),
    gutil        = require('gulp-util'),
    handlebars   = require('gulp-compile-handlebars'),
    imagemin     = require('gulp-imagemin'),
    jshint       = require('gulp-jshint'),
    jsonConcat   = require('gulp-json-concat'),
    merge        = require('merge-stream'),
    path         = require('path'),
    rename       = require('gulp-rename'),
    runSequence  = require('run-sequence'),
    sass         = require('gulp-sass'),
    sprite       = require('gulp-svgstore'),
    svgToSss     = require('gulp-svg-to-css'),
    uglify       = require('gulp-uglify'),
    dataPath     = 'src/data', // set directory for the json data files
    configPath   = 'src/config', // set directory for the json site config files
    env          = 'de-young';

/**
 * Fetch all folder names inside a specific directory (dir)
 */
function getFolders(dir) {
  return fs.readdirSync(dir)
    .filter(function(file) {
      return fs.statSync(path.join(dir, file)).isDirectory();
    });
}

/**
 * Set environment variables
 */
gulp.task('env', function() {
  env = argv.env || 'de-young';
  console.log('-------------------------------------------');
  console.log('\tBUILDING FOR ' + env.toUpperCase());
  console.log('-------------------------------------------');
});

gulp.task('env:de-young', function() {
  env = 'de-young';
});

gulp.task('env:legion-of-honor', function() {
  env = 'legion-of-honor';
});

gulp.task('env:famsf', function() {
  env = 'famsf';
});

/**
 *
 */
gulp.task('var', function() {
  return gulp.src('src/assets/scss/variables/_variables-' + env + '.scss')
    .pipe(rename('_variables.scss'))
    .pipe(gulp.dest('src/assets/scss/settings'));
});

/**
 * Make sure JSON directory is existing and readey for writing
 */
gulp.task('init', function() {
  return fs.stat(dataPath, function(err, stats) {
    // Check if error defined and the error code is "not exists"
    if (err && err.errno === 34) {
      // Create the directory, call the callback.
      fs.mkdir(directory);
    }
  });
});

/**
 * Concatenate JSON files into files relative to their immediate parent directory, prefixed with '-data.json'
 */
gulp.task('json', function() {
  var folders = getFolders(dataPath);

  // go through each directory inside 'folders' (line: 49)
  return folders.map(function(folder) {

    // go through all the json files inside 'folders' (line: 49)
    // and concatenate all of it into a single json file named <directory>-data.json (line)
    return gulp.src([path.join(dataPath, folder, '/**/*.json'), '!/**/*-data.json', '!/**/*-content.json'])
      .pipe(jsonConcat(folder + '-data.json', function(data) {
        return new Buffer(JSON.stringify(data));
      }))
      .pipe(gulp.dest(dataPath));
  });
});

/**
 * Concatenate all json files into one site-content.json
 */
gulp.task('json:site', function() {
  return gulp.src(path.join(dataPath, '/*-data.json'))
    .pipe(jsonConcat('site-content.json', function(data) {
      return new Buffer(JSON.stringify(data));
    }))
    .pipe(gulp.dest(dataPath));
});

/**
 * Generate HTML pages from .hbs templates using data from main JSON file
 * Dependent on task 'json' to generate main JSON file
 */
gulp.task('handlebars', function() {
  var options = {
        batch: ['src/templates/partials', 'src/templates/modules', 'src/templates/objects']
      },
      siteData = JSON.parse(fs.readFileSync(dataPath + '/site-content.json')),
      configData = JSON.parse(fs.readFileSync(configPath + '/' + env + '.json')),
      globalData = JSON.parse(fs.readFileSync(dataPath + '/global.json'));

  for (var configKey in configData) {
    siteData['home-data']['home'][configKey] = configData[configKey];
  }

  fs.readdir('src/templates/pages', (err, files) => {
    files.forEach(file => {
      var page = file.split('.')[0];
      var data = JSON.parse(fs.readFileSync(dataPath + '/' + page + '-data.json'));

      for (var key in data) {
        var fileName = page;

        // if (data.hasOwnProperty(key)) {
        //   fileName = data[key]['slug'];
        // }

        for (var configKey in configData) {
          data[key][configKey] = configData[configKey];
        }

        data[key].global = globalData;

        gulp.src('src/templates/pages/' + page + '.hbs')
          .pipe(handlebars(data[key], options))
          .pipe(rename(fileName + '.html'))
          .pipe(gulp.dest('dist-' + env));
      }
    });
  });

  // create tasks like this for one-off pages
  gulp.src('src/templates/pages/home.hbs')
    .pipe(handlebars(siteData['home-data']['home'], options))
    .pipe(rename('home.html'))
    .pipe(gulp.dest('dist-' + env));
});

/**
 * Copy images to dist
 */
gulp.task('images', function() {
  return gulp.src([
      'src/assets/img/*.{jpg,png,gif,gifv,svg,ico,cur}',
      'src/assets/img/uploaded/*.{jpg,png,gif,gifv,svg,ico,cur}',
      'src/assets/img/' + env + '/*.{jpg,png,gif,gifv,svg,ico,cur}'
    ])
    .pipe(gulp.dest('dist-' + env + '/img'));
});
gulp.task('uploaded', function() {
  return gulp.src([
      'src/assets/img/uploaded/*.{jpg,png,gif,gifv,svg}'
    ])
    .pipe(gulp.dest('dist-' + env + '/img/uploaded'));
});

gulp.task('sprite', function() {
  return gulp.src(['src/assets/img/icons/*.svg', 'src/assets/img/' + env + '/icons/*.svg'])
    .pipe(imagemin({
      svgoPlugins: [{
        removeUnknownsAndDefaults: false
      }]
    }))
    .pipe(sprite({
      inlineSvg: true
    }))
    .pipe(cheerio(function($) {
      $('svg').attr('style', 'display:none');
    }))
    .pipe(gulp.dest('dist-' + env + '/img/icons'));
});

/**
 * Copy fonts to dist
 */
gulp.task('fonts', function() {
  return gulp.src('src/assets/fonts/*.{eot,ttf,woff,woff2}')
    .pipe(gulp.dest('dist-' + env + '/fonts'));
});

/**
 * JS debugging
 */
gulp.task('lint', function() {
  return gulp.src('src/assets/js/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});

/**
 * Concatenate JS
 */
gulp.task('scripts', ['lint'], function() {
  gulp.src([
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
      'node_modules/slick-carousel/slick/slick.min.js',
      'node_modules/waypoints/lib/jquery.waypoints.min.js',
      'node_modules/dragdealer/src/dragdealer.js',
      'node_modules/moment/min/moment.min.js',
      'node_modules/underscore/underscore-min.js',
      'node_modules/clndr/src/clndr.js',
      'src/assets/vendors/js/**/*.js'
    ])
    .pipe(concat('dependency.js'))
    .pipe(gulp.dest('dist-' + env + '/js'));

  gulp.src([
      '!src/assets/js/main-calendar.js',
      'src/assets/js/main.js',
      'src/assets/js/**/*.js'
    ])
    .pipe(concat('script.js'))
    .pipe(gulp.dest('dist-' + env + '/js'));
});

/**
 * Compile CSS
 */
gulp.task('svg', function() {
  return gulp.src('src/assets/img/icons/*.svg')
    .pipe(svgToSss('_svg.scss'))
    .pipe(gulp.dest('src/assets/scss/objects/'));
});

gulp.task('css', function() {
  return gulp.src([
      'node_modules/magnific-popup/dist/magnific-popup.css',
      'node_modules/slick-carousel/slick/slick.css',
      'src/assets/scss/**/*.scss',
      '!src/assets/scss/variables/*.scss'
    ])
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('main.css'))
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'ios_saf 8', 'IE > 9'],
      cascade: false
    }))
    .pipe(gulp.dest('dist-' + env + '/css/'))
    .pipe(browserSync.stream());
});

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: "./dist-de-young/"
    },
    xip: true
  });
});

/**
 * Watch for changes
 * - defaults to de-young
 */
gulp.task('watch', function() {
  browserSync.init({
    server: {
      baseDir: './dist-de-young/'
    },
    xip: true
  });

  gulp.watch('src/assets/js/**/*.js', ['env', 'var', 'lint', 'scripts']);
  gulp.watch('src/**/*.hbs', ['env', 'var', 'handlebars', 'css', 'images', 'sprite', 'scripts', 'fonts']);
  gulp.watch('src/**/*.json', ['env', 'var', 'json', 'handlebars', 'css', 'images', 'sprite', 'scripts', 'fonts']);
  gulp.watch('src/assets/scss/**/*.scss', ['env', 'var', 'css']);
  gulp.watch('src/assets/images/icons/*.svg', ['env', 'var', 'sprite']);
  gulp.watch('src/assets/images/uploaded/*.svg', ['env', 'var', 'uploaded']);

  gulp.watch('dist-de-young/*.html').on('change', browserSync.reload);
});

gulp.task('watch:loh', function() {
  browserSync.init({
    server: {
      baseDir: './dist-legion-of-honor/'
    },
    xip: true
  });

  gulp.watch('src/assets/js/**/*.js', ['env:legion-of-honor', 'var', 'lint', 'scripts']);
  gulp.watch('src/**/*.hbs', ['env', 'var', 'handlebars', 'css', 'images', 'sprite', 'scripts', 'fonts']);
  gulp.watch('src/**/*.json', ['env', 'var', 'json', 'handlebars', 'css', 'images', 'sprite', 'scripts', 'fonts']);
  gulp.watch('src/assets/scss/**/*.scss', ['env:legion-of-honor', 'var', 'css']);
  gulp.watch('src/assets/images/icons/*.svg', ['env:legion-of-honor', 'var', 'svg', 'sprite']);
  gulp.watch('src/assets/images/uploaded/*.svg', ['env', 'var', 'uploaded']);

  gulp.watch('dist-legion-of-honor/*.html').on('change', browserSync.reload);
});

gulp.task('watch:famsf', function() {
  browserSync.init({
    server: {
      baseDir: './dist-famsf/'
    },
    xip: true
  });

  gulp.watch('src/assets/js/**/*.js', ['env:famsf', 'var', 'lint', 'scripts']);
  gulp.watch('src/**/*.hbs', ['env', 'var', 'handlebars', 'css', 'images', 'sprite', 'scripts', 'fonts']);
  gulp.watch('src/**/*.json', ['env', 'var', 'json', 'handlebars', 'css', 'images', 'sprite', 'scripts', 'fonts']);
  gulp.watch('src/assets/scss/**/*.scss', ['env:famsf', 'var', 'css']);
  gulp.watch('src/assets/images/icons/*.svg', ['env:famsf', 'var', 'svg', 'sprite']);
  gulp.watch('src/assets/images/uploaded/*.svg', ['env', 'var', 'uploaded']);

  gulp.watch('dist-famsf/*.html').on('change', browserSync.reload);
});

/**
 * Default tasks for CLI
 */
gulp.task('default', ['build', 'watch']);
gulp.task('build', ['env', 'var', 'handlebars', 'css', 'images', 'uploaded', 'sprite', 'scripts', 'fonts']);




/*
  Explicitly for https://codeandtheory.atlassian.net/browse/FAMSF-419

  - To create dist-calendar/ folder
  - require dist-de-young/ folder to copy files

*/
gulp.task('calendar-clean' , function() {
  return del('dist-calendar/**', {force:true});
});
gulp.task('calendar-build', ['calendar-clean'], function() {

  var cal = {
    dist : 'dist-calendar'
  };
  // js dependency
  gulp.src([
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/moment/min/moment.min.js',
      'node_modules/underscore/underscore-min.js',
      'node_modules/clndr/src/clndr.js',
      'src/assets/vendors/js/modernizr-custom.js'
    ])
    .pipe(gulp.dest(cal.dist + '/js'));

  // js main calendar custom script
  gulp.src([
      'src/assets/js/main-calendar.js',
      'src/assets/js/utils/utils.js'
    ])
    .pipe(concat('calendar-scripts.js'))
    .pipe(gulp.dest(cal.dist + '/js'));

    // calendar html
    gulp.src('dist-de-young/calendar-module-only.html')
    .pipe(concat('calendar.html'))
    .pipe(gulp.dest(cal.dist));

    // css
    gulp.src('dist-de-young/css/main.css')
    .pipe(gulp.dest(cal.dist + '/css'));

    // img
    gulp.src('dist-de-young/img/icons/icons.svg')
    .pipe(gulp.dest(cal.dist + '/img/icons'));
});
gulp.task('calendar-sync' , ['calendar-build'], function() {
  browserSync.init({
    notify: false,
    port: 4000,
    ghostMode: false,
    server: {
      baseDir: "./dist-calendar/"
    },
    xip: true,
    startPath: '/calendar.html'
  });
});
gulp.task('calendar-serve', ['calendar-build', 'calendar-sync']);
