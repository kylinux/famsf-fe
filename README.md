# famsf-fe
Fine Arts Museums of San Francisco (FAMSF) FE Code

## Requirements
* node v6.0.0+

## Setup
1. Clone project repo from git@github.com:codeandtheory/famsf-fe.git
2. Run npm install
3. You are good to go.

## Structure
The `src` directory contains source files for both versions of the site. Both versions share the same codebase except they each has their own image folder, _variable.scss and config json file, `img/de-young`, `_variables-de-young.scss`, `config/de-young.json`, `img/legion-of-honor`, `_variables-legion-of-honor.scss` and `config/legion-of-honor.json`, which manage the variations between the two versions of the site.

## Build
* Run `npm run build:dy` to build for De Young
* Run `npm run build:loh` to build for Legion of Honor
* Run `npm run build` to build for both versions

## HTML
While not a requirement, let's try our best to make sure the markup is valid and satisfy WCAG 2.0 AA criteria.

## JS
Please following the convention here: https://bitbucket.org/mediacurrent/mis_js_standard

## CSS
We will be using BEM for CSS naming convention.
http://getbem.com/introduction/

## Implementation notes

Comments throughout the templates detail specific requirements that may not be immediately apparent or clear. These are also outlined below, with details available in each respective module's comments. Some modules which have already been delivered have updated comments, also noted below.

### Global
**Page Classes**
Each page has a CSS class attached to the `<main>` element matching the page title. This is to facilitate minor styling differences on similar modules from page to page. The exception is the home page; a class can be added here if desired, but isn’t necessary.

**Scroll Animation**
Each module can be made to fade and slide into place as it scrolls into view. This can be enabled on any module by applying the `.transition` CSS class to the module container. Example: `modules/article.hbs` (and most other modules).

### Modules

**article.hbs** (comments modified)
This is a WYSIWYG module, and most elements do not need extra CSS classes etc. The exception to this rule is styled links for external sites and file downloads. Both need a CSS class and an inline SVG, and no whitespace between text and SVG. Examples at lines 13 and 38. This module also includes a block of content at the bottom that’s only for FE dev and QA testing; this can be deleted, and is clearly commented.

**circulation.hbs**
This module has different centering logic depending on the number of items, requiring a CSS class (`.two-up`) when the module only has two elements.

**data-attributes.hbs** (comments modified)
This is a hidden div with data attributes containing icon paths needed by certain JS modules, to avoid hardcoding any file paths in JS. These should be modified to use the correct production paths, and will be picked up by the JS.

**exhibition-hero.hbs** (comments modified)
Media type can be image or video. Implementation details are documented in `objects/media-overlay-button.hbs`

**join-give-hero.hbs** (comments modified)
Join/Give tabs need a corresponding `.active` class for the tab matching the page being viewed. Example at line 18.

**join-give-intro.hbs** (comments modified)
This is another WYSIWYG module; external links need a CSS class and inline SVG. Example at line 12.

**manifesto-dy.hbs** (comments modified)
Each link has a corresponding rollover image, matched via data attributes. Details and examples are in the template. Note that this only applies to the DeYoung instance of this module; images are not present for Legion of Honor.

**press.hbs** (comments modified)
In order to keep the external link icon from breaking to its own line, there are some markup requirements, as detailed in the template.

**related-events.hbs** (comments modified)
The number of events listed must be present in a CSS class. Example and details at lines 24-25.

**related-stories.hbs**
The number of stories listed must be present in a CSS class. Example and details at lines 16-17.
