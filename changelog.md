# Change Log

### Maintenance Code Drop 2017-4-4

### 2017-4-4

Re-add `grab-hover__follow` DOM element in related-stories, related-events.hbs, re-add JS to drag.js, re-add styles to _drag.scss

New module `related-events-small.hbs` for 1-2 events, new styles in _related-events.scss module.

### Sprint 4 Code Drop 2016-12-20

## Sprint 1 update

### 2016-12-16

Add Pinterest icon to Global footer
https://codeandtheory.atlassian.net/browse/FAMSF-347

Move `.transition` class to src/templates/modules/feature.hbs to src/templates/objects/collection.hbs to correct scrolling animation behavior.
https://codeandtheory.atlassian.net/browse/FAMSF-346

Remove `grab-hover__follow` DOM element from related-stories, relatede-evnts.hbs, remove JS from drag.js, remove styles from _drag.scss

### Sprint 3 Code Drop 2016-12-13

## Sprint 2 Update

### 2016-12-12
src/modules/circulation.hbs: remove errant class from module (line 3)

### 2016-12-08
In hero.hbs -> media.block2 - inserted new DOM element with class 'media-bg__img', moved background image to that element. Added styling to allow this DOM element to black and white, without affecting the color-overlay child.
* L16 in hero.hbs
* L94 in _hero.scss

### Sprint 2a Code Drop 2016-12-06

## Sprint 2 Update
### 2016-12-05

In files related-events.hbs and related-stories.hbs - moved class 'grab-hover__follow' element outside of parent container to avoid being touched by waypoints.js (for y-pos math).
  * on line 2 (both files)

## Sprint 1 Update
### 2016-12-05

Change Media Gallery ratio to 16:10 (FAMSF-243)
https://codeandtheory.atlassian.net/browse/FAMSF-243

* src/config/de-young.json, src/config/legion-of-honor.json
  * on line 473, change value of heroLink from [ u-bgc-grey-lt u-opacity-80 ] to [ u-bgc-grey-lt u-opacity-70 ]
* dist-de-young/index.html, dist-legion-of-honor/index.html (just a reference on the json updates)
  * on line 1012, change class "u-opacity-80" to "u-opacity-70"

Add Buy link to mobile navigation (FAMSF-246)
https://codeandtheory.atlassian.net/browse/FAMSF-246

* src/config/de-young.json, src/config/legion-of-honor.json
  * on line 453, add Buy Tickets data
* src/templates/modules/header.hbs
  * on lines 20 and 49, change `<li class="header__nav-item">` to `<li class="header__nav-item {{this.class}}">`
* dist-de-young/index.html, dist-legion-of-honor/index.html (a reference on the json and hbs updates)
  * on line 107, a new list was added under `<li class="header__nav-item buy">`

Add hover to images in Global navigation
https://codeandtheory.atlassian.net/browse/FAMSF-272

* src/templates/modules/navigation.hbs

### Sprint 2 code drop

### 2016-12-02

Fix invalid HTML
https://codeandtheory.atlassian.net/browse/FAMSF-264

* src/templates/modules/collection.hbs
  * Change `<h1>` to `<h2>` line 6
  * Remove `alt` attribute from `<a>` tag line 11
  * Change `<h1>` to `<h3>` line 36
* src/templates/modules/editorial.hbs
  * Remove extra closing `</h2>` line 21
* src/templates/modules/breaker.hbs
  * Change `<h1>` to `<h2>` line 10
* src/templates/modules/footer.hbs
  * Change `<h1>` to `<h2>` line 9


Remove hardcoded SVG paths from JS
https://codeandtheory.atlassian.net/browse/FAMSF-265

* src/templates/modules/data-attributes.hbs (NEW)
* src/templates/partials/bottom.hbs
  * Add module include at line 2
* dist-de-young/index.html, dist-legion-of-honor/index.html (affected pages)

Fix extra spacing on Editorial module due to hidden scrollbars (FAMSF-256)
https://codeandtheory.atlassian.net/browse/FAMSF-256
* src/templates/modules/editorial.hbs
	* on line 6, the container "editorial__scroll" is placed inside the container "editorial__scroll--container"
* dist-de-young/index.html, dist-legion-of-honor/index.html (just a reference on the .hbs updates)
	* on line 1027, added a new div container "editorial__scroll--container" (with a corresponding end div tag) and placed the whole "editorial__scroll" div container inside it.

### 2016-12-01

Update footer link (FAMSF-233)
https://codeandtheory.atlassian.net/browse/FAMSF-233
* src/templates/modules/footer.hbs
	*
	```
	on line 52, added anchor tag, changed '<h2>Fine Arts Museum <span>of San Francisco</span></h2>' to '<h2><a href="/" title="" alt="">Fine Arts Museum of San Francisco</a></h2>'
	```
* dist-de-young/index.html, dist-legion-of-honor/index.html (just a reference on the .hbs updates)
	* on line 1039, added anchor tag (see above)

### 2016-11-24

Modify padding of Announcement module inside the Breaker module (FAMSF-69)
https://codeandtheory.atlassian.net/browse/FAMSF-69
* src/templates/modules/breaker.hbs
	* on line 9, added a new class
* dist-de-young/index.html, dist-legion-of-honor/index.html (just a reference on the .hbs updates)
	* on line 980, removed "[ u-df u-aic ]" class
	* on line 986, added "u-span11@l u-offset1@l" class
