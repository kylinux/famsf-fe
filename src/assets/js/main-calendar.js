/*
  Explicitly for https://codeandtheory.atlassian.net/browse/FAMSF-419

  This is a clean copy of main.js but for calendar module only
*/
var FAMSF = {};

(function($) {

  FAMSF = {
    $document: $(document),
    $window: $(window),
    $body: $('body'),
    // All pages
    'common': {
      init: function () {
        FAMSF.calendar.init();
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = FAMSF;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

  jQuery.fn.reverse = [].reverse;

})(jQuery); // Fully reference jQuery after this point.



FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.calendar = {
    init: function () {
      FAMSF.calendar.loadCalendar();
      FAMSF.calendar.loadWaypoints();

      $(window).on('resize', function() {
        FAMSF.calendar.updateElements();
      });

      $('.js-calendar--fixed').on('click', '.js-calendar--open, .js-calendar--close', function(e) {
        e.preventDefault();
        FAMSF.calendar.toggle();
      });

      $(document).on('click', function(e) {
        if (!$(e.target).parents('.js-calendar--fixed').length &&
          !$(e.target).parents('.js-calendar--prev, .js-calendar--next').length &&
          !$(e.target).parents('.js-calendar--day').length) {
          $('.js-calendar--fixed').removeClass('calendar--open');
        }
      });
    },

    _loadCalendar: function($calendar) {
      var $calendarContainer = $('.js-calendar--container');
      var $calendarWrapper   = $('.js-calendar--wrapper');
      var $eventsListing   = $('.events-listing');

    },

    loadCalendar: function() {
      this.clndrInline = this._loadCalendar( $('.js-calendar--inline').find('.js-calendar--wrapper') );
      this.clndrFixed  = this._loadCalendar( $('.js-calendar--fixed').find('.js-calendar--wrapper') );
    },

    toggle: function() {
      $calendar = $('.js-calendar--fixed');

      if ($calendar.hasClass('calendar--open')) {
        $calendar.removeClass('calendar--open');

        if (typeof FAMSF.calendar.clndrFixed !== 'undefined') {
          FAMSF.calendar.clndrFixed.setMonth( FAMSF.calendar.selectedDate.month() );
          FAMSF.calendar.clndrFixed.setYear( FAMSF.calendar.selectedDate.year() );
        }
      }
      else {
        $calendar.addClass('calendar--open');
      }
    },

    loadWaypoints: function() {
      $(window).on('load resize scroll', this.updateWaypoints);
    },

    updateWaypoints: function() {
      var $w               = $(window);
      var $calendar        = $('.js-calendar');
      var $calendarFixed   = $('.js-calendar--fixed');
      var $calendarWrapper = $calendarFixed.find('.js-calendar--wrapper');
      var $eventsListing   = $('.events-listing');

      // Tablet+.
      if (FAMSF.utils.getBreakpoint() !== 'small') {
        if ($('.events-listing-blocks .block--item').length === 0) {
          $calendarFixed
            .removeClass('calendar--show')
            .addClass('calendar--hidden');
          return false;
        }

        var headerHeight     = $('.js-header').outerHeight();
        var top              = (($w.height() + headerHeight) * 0.5) - ($calendarWrapper.outerHeight() * 0.5);
        var topTriggerPoint  = $calendar.outerHeight();
        var hideTriggerPoint = $eventsListing.offset().top + $eventsListing.outerHeight(true) - $calendarWrapper.outerHeight() - top;

        // Hides widget.
        if ($w.scrollTop() >= hideTriggerPoint || $w.scrollTop() < topTriggerPoint) {
          $calendarFixed
            .removeClass('calendar--show')
            .addClass('calendar--hidden');
        }
        // Sticks widget vertically centered and fixed.
        else  {
          $calendarFixed
            .removeClass('calendar--hidden')
            .addClass('calendar--show')
            .css({ 'top': top });
        }
      }
      // Mobile.
      else {
        $calendarFixed.removeAttr('style');
      }
    },

    updateElements: function() {

      if (FAMSF.utils.getBreakpoint() !== 'small') {
        $('.js-calendar--inline, .js-calendar--fixed').each(function() {
          var $calendar = $(this);
          var wrapHeight = $calendar.find('.js-calendar--wrapper').outerHeight();
          wrapHeight   -= $calendar.hasClass('js-calendar--inline') ? $('.js-header').height() : 0;
          $calendar.find('.js-calendar--title').width( wrapHeight );

          $calendar.find('.js-calendar--nav').height( $calendar.find('.js-calendar--grid').height() );
        });
      }
      else {
        $('.js-calendar--title, .js-calendar--nav').removeAttr('style');
      }
    }

  };
})(jQuery);
