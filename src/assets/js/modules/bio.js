FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.bio = {
    init: function () {
      var $context = $('.js-bio');


      $.each($context, function(){
        var $this     = $(this);
        var $btn      = $('.js-bio-btn', $this);
        var $content  = $('.collection__body', $this);
        var $inner    = $('.collection__body-inner', $this);
        var _$inner   = { height : $inner.height() };
        var $gallery  = $('.collection__gallery', $this);

        var _label = {
          expanded  : 'Show Less -',
          collapsed : 'Show More +'
        };

        var getInnerHeight = function(){
          _$inner.height = $inner.height();
          $('.is-expanded', $this).css('max-height', _$inner.height);
        };

        // events
        $btn.on('click', function(e){
          e.preventDefault();

          if ( $content.hasClass('is-expanded')){
            $btn
              .text( _label.collapsed);
            $this
              .removeClass('is-expanded');
            $content
              .removeAttr('style')
                .removeClass('is-expanded');
            

            $("html, body")
              .animate({ scrollTop: $this.offset().top });
            
          } else {
            $btn
              .text( _label.expanded );
            $this
              .addClass('is-expanded');
            $content
              .css('max-height', _$inner.height)
              .addClass('is-expanded');
            
            
            $(window)
              .lazyLoadXT();
          }

          setTimeout(function(){
            FAMSF.transition.refresh();
          }, 800);
          
        });

        FAMSF.eventHandler.subscribe('EVENT_RESIZE_INSTANT', getInnerHeight);
      });
    }
  };
})(jQuery);