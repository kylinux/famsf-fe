FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.joinGiveIntro = {
    $header: $('.header'),
    $intro: $('.join-give-intro'),
    $introMain: $('.join-give-intro__main'),
    $sidebar: $('.join-give-intro__aux'),
    sidebarHeight: 0,
    introHeight: 0,
    introMainHeight: 0,
    scrollPosition: 0,
    sidebarPosition: 0,
    scrollAnimOffset: 100, // number of pixels a .transition element travels when fading/scrolling in
    sidebarFixedTop: 105, // must have the same top value in css .join-give-intro__aux.fixed per breakpoint

    initSidebar: function() {
      var self = FAMSF.joinGiveIntro;

      self.sidebarHeight = self.$sidebar.outerHeight();
      self.introHeight = self.$intro.outerHeight();
      self.introMainHeight = self.$introMain.outerHeight();
      self.scrollPosition = $(window).scrollTop();

      if (self.$intro.hasClass('transition--completed')) {
        self.sidebarPosition = self.$intro.offset().top;
      } else {
        self.sidebarPosition = self.$intro.offset().top - self.scrollAnimOffset;
      }
    },

    dockSidebar: function() {
      var self = FAMSF.joinGiveIntro;

      if (self.$intro.hasClass('transition--completed')) {
        self.sidebarPosition = self.$intro.offset().top;
      } else {
        self.sidebarPosition = self.$intro.offset().top - self.scrollAnimOffset;
      }

      // update sidebarFixedTop value per breakpoint
      if (FAMSF.utils.getBreakpoint() === 'medium') {
        self.sidebarFixedTop = 140; // value is from .join-give-intro__aux.fixed { top: value }
      }

      if (FAMSF.utils.getBreakpoint() === 'large' || FAMSF.utils.getBreakpoint() === 'xlarge') {
        self.sidebarFixedTop = 105; // value is from .join-give-intro__aux.fixed { top: value }
      }


      if (self.introMainHeight <= self.sidebarHeight) {
        return; // if right side is taller than left side, no need to dock
      }

      if (self.$intro.hasClass('transition--completed')) {
        self.scrollPosition = $(window).scrollTop();

        if (self.scrollPosition > self.sidebarPosition - self.sidebarFixedTop) {
          // define where we want the sidebar to start scrolling out of view
          var targetPos = self.sidebarPosition + self.introHeight - self.sidebarHeight - self.sidebarFixedTop;

          if (self.scrollPosition > targetPos) {
            self.$sidebar.removeClass('fixed').addClass('scrolled');

          } else {
            self.$sidebar.removeClass('scrolled').addClass('fixed');
          }
        } else {
          self.$sidebar.removeClass('fixed scrolled');
        }
      }
    },

    init: function() {
      var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);

      if (navigator.userAgent.match(/(iPod|iPhone|iPad)/) ) {
          $('.join-give-intro__aux').addClass('ios-device');
          return false;
      }

      if (FAMSF.utils.getBreakpoint != 'small') {
        var self = FAMSF.joinGiveIntro;

        // timeouts are to let .transition behaviors complete so we don't get incorrect offset() values
        window.setTimeout(self.initSidebar, 200);
        window.setTimeout(self.dockSidebar, 200);

        FAMSF.eventHandler.subscribe('EVENT_RESIZE', function () {
          self.initSidebar();
          self.dockSidebar();
        });

        FAMSF.eventHandler.subscribe('EVENT_SCROLL_INSTANT', function () {
          self.dockSidebar();
        });
      }
    }
  };
})(jQuery);
