FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.newsletter = {
    init: function () {
      var module = FAMSF.newsletter;

      var $context = $('.js-newsletter');
      var $form = $('form', $context);
      var $email = $('input[name="email"]', $context);
      var _msg = {
        tmpl : '<span class="textfield__msg">_msg_</span>',
        invalid  : "Invalid email address.",
        timeout : "Something went wrong. Try again.",
        success : "Thanks for signing up."
      };



      $email.on('keyup', function(){
        if ( $email.val().length ) { $email.parent().addClass('is-filled'); }
        else {
          $email.parent().removeClass('is-filled');
        }
      }).on('focus', function(){
        $email.parent().addClass('is-focus');
      }).on('blur', function(){
        $email.parent().removeClass('is-focus');
      });

      $form.on('submit', function(event){
        event.preventDefault();
        // Serialize the form data.
        var formData = $form.serialize();

        $('.textfield__msg', $context).remove();

        // validate email format
        if ( !/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm.test( $email.val() ) ){
          if ( !$email.next('.textfield__msg').length ){
            $(_msg.tmpl).insertAfter($email);
          }
          $email.next().text(_msg.invalid);
          return;
        }

        if ( !$email.next('.textfield__progress').length ){
          $('<div class="textfield__progress"><span class="textfield__progress-bar"></span></div>').insertAfter($email);
        }

        $email.attr('disabled', true);


        var _progress = {
          increment : 0,
          interval : 25,
          max: 800
        };
        var progress = function(){
          _progress.increment += _progress.interval;
          var width = _progress.increment / _progress.max *100;

          $('.textfield__progress-bar', $context).width( width.toFixed(2) + '%' );
          if ( _progress.increment == _progress.max ){
            clearInterval(interval);
            $('.textfield__progress', $context).remove();
            $(_msg.tmpl).insertAfter($email);
            $email.next().text(_msg.success);
            $email.removeAttr('disabled');
          }
        };
        var interval = setInterval(progress, _progress.interval);
      });
    }
  };
})(jQuery);
