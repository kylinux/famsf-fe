FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.permanentCollections = {
    init: function () {
      var $context = $('.js-permanent-collections');


      $.each($context, function(){
        var $this = $(this);
        var $collections = $('a[data-img]', $this);

        $('.collection__hero-link', $this).attr('disabled', true);
        
        var setCollection = function( $elem ){
          $('.collection__hero .media-bg', $this).css('background-image', 'url(' + $elem.data('img') +')');
          $('.collection__hero-link', $this).attr('href', $elem.attr('href'));
          $('.collection__hero-link', $this).attr('disabled', false);

          $collections.removeClass('is-active');
          $elem.addClass('is-active');
        };
        
        // events
        $collections.on('mouseover', function(e){
          var $collection = $(this);
          setCollection($collection);
        }); 
      });
    }
  };
})(jQuery);