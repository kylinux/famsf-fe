FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.membershipLevel = {
    init: function () {
      var ML = this;
      var $context = $('.js-membership-level');
      var $btn = $('.js-show-btn', $context);


      $btn.on('click', function(e){
        e.preventDefault();
        var $this     = $(this),
            $item     = $this.parents('.js-item'),
            $content  = $('.js-item-content', $item);

        if ( $this.hasClass('is-expanded')) {
          $this
            .removeClass('is-expanded');

          $content
            .css('max-height', $content.attr('data-min-height') + 'px')
            .removeClass('is-expanded');

          $("html, body")
            .animate({ scrollTop: $item.offset().top - $('header').outerHeight() }, 1000);
        } else {
          $this
            .addClass('is-expanded');

          $content
            .css('max-height', $('.js-item-content-inner', $content).outerHeight())
            .addClass('is-expanded');

        }
        ML.setBtnLabel();

        // disable on click, enabled when content complete to expanded/collapsed
        $this.attr('disabled', true);
        setTimeout(function(){
          $this.attr('disabled', false);
        }, 1000);
      });

      ML.tabHandler();
      ML.contentHeightHandler();
      ML.urlFragmentHandler();
      ML.resizeHandler();
    },

    setBtnLabel: function() {
      var $context = $('.js-membership-level');
      var $btn = $('.js-show-btn', $context);

      var _label = {
        expanded  : 'Show less benefits -',
        collapsed : 'Show more benefits +',
        mobile : {
          expanded  : 'Hide benefits -',
          collapsed : 'Show benefits +',
        }
      };

      if ( FAMSF.utils.getBreakpoint() === "small" ){
        $btn.not('.is-expanded').text(_label.mobile.collapsed);
        $btn.filter('.is-expanded').text(_label.mobile.expanded);
      } else {
        $btn.not('.is-expanded').text(_label.collapsed);
        $btn.filter('.is-expanded').text(_label.expanded);
      }
    },
    
    tabHandler: function(){
      var ML = this;
      var $context = $('.js-membership-level');
      var $btn = $('.js-tab-btn', $context);

      $btn.on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        
        $btn
          .removeClass('is-active');

        $this
          .addClass('is-active');

        window.location.hash = $this.attr('href');
        ML.urlFragmentHandler();
      });
    },
    urlFragmentHandler: function(){
      var hashValue = window.location.hash.substr(1),
          $context  = $('.js-membership-level'),
          $tabBtn   = $('.js-tab-btn', $context);
          $tabItem  = $('.js-tab-item', $context);

      if (hashValue) {
        $tabBtn.removeClass('is-active');
        $tabItem.removeClass('is-active');

        $tabBtn.filter('[href="#' + hashValue + '"]').addClass('is-active');
        $tabItem.filter('[data-tab-id="' + hashValue + '"]').addClass('is-active');

        $('.media-bg', $context).lazyLoadXT();
      }
    },
    contentHeightHandler : function(){
      var $context  = $('.js-membership-level'),
          $item     = $('.js-item', $context);

      
      $.each($item, function(){
        var $this = $(this), 
            $content  = $('.js-item-content', $this),
            $list = $('ul', $content),
            $listItem = $('li', $content),
            $btn = $('.js-show-btn', $this),
            minHeight = 0,
            maxHeight = $('.js-item-content-inner', $content).outerHeight(),
            maxItem = 2;

        if ( $listItem.length > maxItem ) { 
          minHeight = $listItem.filter(':nth-child('+ (maxItem+1) +')').position().top;
        } else {
          $btn.hide();
        }
          
        if ( FAMSF.utils.getBreakpoint() === "small" ){
          $btn
            .removeAttr('style');
          minHeight = 0;
        } else {
          if ( $listItem.length <= maxItem ) {
            minHeight = 'none';
          }
        }

        $content
          .attr('data-min-height', minHeight);

        if ( !$btn.hasClass('is-expanded')){
          $content
            .css('max-height', minHeight);
        } else {
          $content
            .css('max-height', maxHeight);
        }
      });

    },
    resizeHandler: function() {
      var ML = this;

      FAMSF.eventHandler.subscribe('EVENT_RESIZE_INSTANT', function(){
        ML.setBtnLabel();
        ML.contentHeightHandler();
      });
    }

  };
})(jQuery);