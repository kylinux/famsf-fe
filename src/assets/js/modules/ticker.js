FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.ticker = {
    init: function () {
      setTimeout(function() {
        var $ticker = $('.ticker');
        var $tickerInner   = $('.ticker__inner', $ticker);
        var $tickerTime    = $('.ticker__time', $ticker);
        var offsetLeft     = $ticker.width() - $tickerTime.width();
        var timeMultiplier = 20;

        $ticker.addClass('ticker--animating');
        $tickerInner
          .css('left', offsetLeft)
          .animate({
            left: -$tickerInner.width()
          }, {
            duration: $tickerInner.width() * timeMultiplier,
            easing: 'linear',
            complete: function() {
              offsetLeft = $ticker.width() - $tickerTime.width();

              $tickerInner
                .css('left', '100%')
                .animate({
                  left: offsetLeft
                }, {
                  duration: $tickerTime.width() * timeMultiplier,
                  easing: 'linear',
                  complete: function() {
                    $ticker.removeClass('ticker--animating');
                    $tickerInner.css('left', 'auto');
                  }
                });
            }
          });
      }, 3000);
    }
  };
})(jQuery);
