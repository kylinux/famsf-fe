FAMSF = window.FAMSF || {};

(function ($) {
    FAMSF.bodymodule = {
        init: function () {
            var _cta = $('.showmore-cta');
            _cta.on('click',function() {
               if( $(this).is('.clicked')){
                   $(this).html('Show More +');
                   $(this).removeClass('clicked');
                   $(this).prev('.body-module-content').removeClass('open');
               } else {
                   $(this).addClass('clicked');
                   $(this).html('Show Less -');
                   $(this).prev('.body-module-content').addClass('open');
               }
            });
        }
    };
})(jQuery);
