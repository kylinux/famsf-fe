FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.visitNotification = {
    init: function () {
      new FAMSF.Slider($('.visit-notification .slider'), {peeking: false});
    }
  };
})(jQuery);
