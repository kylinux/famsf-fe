FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.map = {
    init: function () {

      var _map = $('.js-map');
      var map = {
        $pin : $('.js-map-pin', _map),
        $media : $('.js-map-media', _map)
      };


      // events
      map.$pin.on('mouseover', function(e){
        var $this = $(this);
        map.$media.css({
          'background-image': 'url(' + $this.data('bgImg') +')',
          'opacity' : '.5'
        });
      });

      map.$pin.on('mouseout', function(e){
        map.$media.css('opacity', '0');
      });
    }
  };
})(jQuery);
