FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.manifesto = {
    animateUnderlines: function() {
      var manifestoScrollPos = $('.manifesto--deyoung').offset().top + 200,
        windowScrollPos = $(window).scrollTop() + $(window).height();

      if (windowScrollPos > manifestoScrollPos) {
        $('.manifesto a').each(function() {
          var $this = $(this),
              delayTime = (Math.floor(((Math.random() * 80) + 1)) * 10);

          window.setTimeout(function(){
            $this.addClass('is-underlined');
          }, delayTime);
        });
      }
    },

    initManifestoImages: function() {
      $('.manifesto__image').each(function() {
        var $this = $(this),
            thisImg = $this.find('img').attr('src');

        $this.css('background-image', 'url("' + thisImg + '")');
      });
    },

    displayManifestoImage: function(el) {
      var imgCounter = el.data('counter'),
          $linkedImg = el.closest('.manifesto').find('.manifesto__image[data-counter=' + imgCounter + ']');

      if ($linkedImg.hasClass('is-visible')) {
        $linkedImg.removeClass('is-visible');
      } else {
        $linkedImg.addClass('is-visible');
      }
    },

    init: function () {
      FAMSF.manifesto.animateUnderlines();
      FAMSF.manifesto.initManifestoImages();

      FAMSF.eventHandler.subscribe('EVENT_SCROLL', function () {
        FAMSF.manifesto.animateUnderlines();
      });

      $('.manifesto a').hover(function() {
        FAMSF.manifesto.displayManifestoImage($(this));
      });
    }
  };
})(jQuery);
