FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.carouselMarquee = {
    init: function () {
      var $carouselMarquee      = $('.carousel-marquee');
      var $carouselMarqueeItems = $('.carousel-marquee__items', $carouselMarquee);

      new FAMSF.Slider($carouselMarqueeItems);

      if ($('.carousel-marquee__item', $carouselMarquee).length === 1) {
        $carouselMarquee.addClass('carousel-marquee--single');
      }
    }
  };
})(jQuery);
