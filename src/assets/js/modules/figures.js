FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.figures = {
    init: function () {
      var prevBreakpoint;
      var slider;

      FAMSF.eventHandler.subscribe('EVENT_RESIZE', function () {
        var currentBreakpoint = FAMSF.utils.getBreakpoint();
        
        if (currentBreakpoint !== prevBreakpoint) {
          if (currentBreakpoint === 'small') {
            slider = new FAMSF.Slider($('.figures .slider'), {peeking: false});
          }
          else if (slider) {
            slider.destroy();
          }
        } 

        prevBreakpoint = currentBreakpoint;
      });
      
      $(window).resize();
    }
  };
})(jQuery);
