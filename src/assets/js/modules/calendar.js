FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.calendar = {
    init: function () {
      FAMSF.calendar.loadCalendar();
      FAMSF.calendar.loadWaypoints();

      $(window).on('resize', function() {
        FAMSF.calendar.updateElements();
      });

      $('.js-calendar--fixed').on('click', '.js-calendar--open, .js-calendar--close', function(e) {
        e.preventDefault();
        FAMSF.calendar.toggle();
      });

      $(document).on('click', function(e) {
        if (!$(e.target).parents('.js-calendar--fixed').length &&
          !$(e.target).parents('.js-calendar--prev, .js-calendar--next').length &&
          !$(e.target).parents('.js-calendar--day').length) {
          $('.js-calendar--fixed').removeClass('calendar--open');
        }
      });
    },

    _loadCalendar: function($calendar) {
      var $calendarContainer = $('.js-calendar--container');
      var $calendarWrapper   = $('.js-calendar--wrapper');
      var $eventsListing   = $('.events-listing');

      return $calendar.clndr({
        template: $('#calender-template').html(),
        showAdjacentMonths: false,
        trackSelectedDate: true,
        daysOfTheWeek: ['S', 'M', 'T', 'W', 'Th', 'F', 'S'],
        targets: {
          nextButton: 'js-calendar--next',
          previousButton: 'js-calendar--prev'
        },
        clickEvents: {
          click: function(target) {
            if ($eventsListing.length) {
              var currentBreakpoint = FAMSF.utils.getBreakpoint();

              if ('small' === currentBreakpoint && $(target.element).parents('.js-calendar--fixed').length) {
                // Mobile should scroll to top
                $('html, body').animate({ scrollTop: 0 }, 200);
              } else if ('small' !== currentBreakpoint && $(target.element).parents('.js-calendar--inline').length) {
                // Tablet+ should scroll to top of Events Listing
                $('html, body').animate({ scrollTop: $('.events-listing').offset().top - 80 }, 200);
              }

              FAMSF.eventsListing.updateListing(target.date);

              // Mobile calendar should retract on date selection.
              if (currentBreakpoint === 'small') {
                $('.js-calendar--fixed').removeClass('calendar--open');
              }
            }

            FAMSF.calendar.selectedDate = target.date;

            // Updates calendar month view.
            if ($(target.element).parents('.js-calendar--fixed').length) {
              FAMSF.calendar.clndrInline.setMonth( FAMSF.calendar.selectedDate.month() );
              FAMSF.calendar.clndrInline.setYear( FAMSF.calendar.selectedDate.year() );
            }
            if ($(target.element).parents('.js-calendar--inline').length) {
              FAMSF.calendar.clndrFixed.setMonth( FAMSF.calendar.selectedDate.month() );
              FAMSF.calendar.clndrFixed.setYear( FAMSF.calendar.selectedDate.year() );
            }

            FAMSF.calendar.updateElements();
          }
        },
        doneRendering: function() {
          if ($eventsListing.length) {
            if ($calendar.find('.calendar__cell.today').length && typeof FAMSF.calendar.selectedDate === 'undefined') {
              var $today   = $calendar.find('.calendar__cell.today');
              var currentDay = moment();
              var classes  = $today.attr('class').split(' ');

              for (var i = 0; i < classes.length; i++) {
                if (classes[i].indexOf('calendar-day') > -1) {
                  currentDay = moment(classes[i].substr(-10), 'YYYY-MM-DD');
                }
              }

              $today.removeClass('today').addClass('selected');
              $('.js-current-date').html(currentDay.format('MMMM D, YYYY'));

              FAMSF.calendar.selectedDate = currentDay;
            }
          }

          FAMSF.calendar.updateElements();
        }
      });
    },

    loadCalendar: function() {
      this.clndrInline = this._loadCalendar( $('.js-calendar--inline').find('.js-calendar--wrapper') );
      this.clndrFixed  = this._loadCalendar( $('.js-calendar--fixed').find('.js-calendar--wrapper') );
    },

    toggle: function() {
      $calendar = $('.js-calendar--fixed');

      if ($calendar.hasClass('calendar--open')) {
        $calendar.removeClass('calendar--open');

        if (typeof FAMSF.calendar.clndrFixed !== 'undefined') {
          FAMSF.calendar.clndrFixed.setMonth( FAMSF.calendar.selectedDate.month() );
          FAMSF.calendar.clndrFixed.setYear( FAMSF.calendar.selectedDate.year() );
        }
      }
      else {
        $calendar.addClass('calendar--open');
      }
    },

    loadWaypoints: function() {
      $(window).on('load resize scroll', this.updateWaypoints);
    },

    updateWaypoints: function() {
      var $w               = $(window);
      var $calendar        = $('.js-calendar');
      var $calendarFixed   = $('.js-calendar--fixed');
      var $calendarWrapper = $calendarFixed.find('.js-calendar--wrapper');
      var $eventsListing   = $('.events-listing');

      // Tablet+.
      if (FAMSF.utils.getBreakpoint() !== 'small') {
        if ($('.events-listing-blocks .block--item').length === 0) {
          $calendarFixed
            .removeClass('calendar--show')
            .addClass('calendar--hidden');
          return false;
        }

        var headerHeight     = $('.js-header').outerHeight();
        var top              = (($w.height() + headerHeight) * 0.5) - ($calendarWrapper.outerHeight() * 0.5);
        var topTriggerPoint  = $calendar.outerHeight();
        var hideTriggerPoint = $eventsListing.offset().top + $eventsListing.outerHeight(true) - $calendarWrapper.outerHeight() - top;

        // Hides widget.
        if ($w.scrollTop() >= hideTriggerPoint || $w.scrollTop() < topTriggerPoint) {
          $calendarFixed
            .removeClass('calendar--show')
            .addClass('calendar--hidden');
        }
        // Sticks widget vertically centered and fixed.
        else  {
          $calendarFixed
            .removeClass('calendar--hidden')
            .addClass('calendar--show')
            .css({ 'top': top });
        }
      }
      // Mobile.
      else {
        $calendarFixed.removeAttr('style');
      }
    },

    updateElements: function() {
      if (FAMSF.calendar.selectedDate !== 'undefined') {
        // Removes modifier classes of current and selected date.
        $('.calendar__cell')
          .removeClass('today')
          .removeClass('selected');

        // Updates the date displayed on calendar widget.
        $('.js-calendar--date').html(FAMSF.calendar.selectedDate.format('MMMM D'));

        // Updates the date displayed on Events Listing filter.
        $('.js-events-filter--date').html(FAMSF.calendar.selectedDate.format('MMMM D, YYYY'));

        // Adds class of the selected date.
        var ymd = FAMSF.calendar.selectedDate.format('YYYY-MM-DD');
        $('.calendar-day-' + ymd).addClass('selected');
      }

      if (FAMSF.utils.getBreakpoint() !== 'small') {
        $('.js-calendar--inline, .js-calendar--fixed').each(function() {
          var $calendar = $(this);
          var wrapHeight = $calendar.find('.js-calendar--wrapper').outerHeight();
          wrapHeight   -= $calendar.hasClass('js-calendar--inline') ? $('.js-header').height() : 0;
          $calendar.find('.js-calendar--title').width( wrapHeight );

          $calendar.find('.js-calendar--nav').height( $calendar.find('.js-calendar--grid').height() );
        });
      }
      else {
        $('.js-calendar--title, .js-calendar--nav').removeAttr('style');
      }
    },

    resetCalendar: function() {
      if ($('.js-calendar').length === 0) {
        return false;
      }

      $('.js-calendar--fixed')
        .removeClass('calendar--open')
        .removeClass('calendar--show')
        .addClass('calendar--hidden');

      this.updateElements();
    },

    navigate: function(momentDate, direction) {
      this.navigateDirection = direction;

      if (direction === 'previous') {
        this.clndrInline.back();
        this.clndrFixed.back();

        $('.js-calendar--inline, .js-calendar--fixed').each(function() {
          $(this).find('[class*=calendar-day]').last().trigger('click');
        });
      }
      else if (direction === 'next') {
        this.clndrInline.forward();
        this.clndrFixed.forward();

        $('.js-calendar--inline, .js-calendar--fixed').each(function() {
          $(this).find('[class*=calendar-day]').first().trigger('click');
        });
      }
    }
  };
})(jQuery);
