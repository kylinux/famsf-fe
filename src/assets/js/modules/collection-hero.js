FAMSF = window.FAMSF || {};

(function ($) {
    FAMSF.collectionHero = {
        init: function () {
            var collection = $('.hero--collection');
            var bgitems = collection.attr('data-item').split(',');

            var images = bgitems;
            $('.media-bg').css({'background-image': 'url('+ images[Math.floor(Math.random() * images.length)] + ')'});
        }
    };
})(jQuery);
