FAMSF = window.FAMSF || {};

(function ($) {
    FAMSF.cinematicGallery = {
        init: function () {

            var _gallery = $('.js-cinematic-gallery'),
                _galleryControls = $('.js-cinematic-gallery-caption'),
                _galleryItems = _gallery.find('.gallery-item'),
                _item_length = $('.js-cinematic-gallery > div').length,
                _item_length_index = _item_length - 1,

                $dataAttributes = $('.data-attributes'),
                _svgSpritePath = $dataAttributes.data('svgSpritePath'),
                _carouselArrowLeft = _svgSpritePath + '#' + $dataAttributes.data('carouselArrowLeft'),
                _carouselArrowRight = _svgSpritePath + '#' + $dataAttributes.data('carouselArrowRight');

            _gallery.on('init', function(event,slick) {
                $('.slick-cloned .media-overlay__item').remove();
            });

            _gallery.slick({
                lazyLoad: 'ondemand',
                infinite: true,
                arrows: true,
                slidesToShow: 1,
                asNavFor: _galleryControls,
                prevArrow: "<button class='slick-prev'><svg role='img'><use xlink:href='" + _carouselArrowLeft + "'></use></svg></button>",
                nextArrow: "<button class='slick-next'><svg role='img'><use xlink:href='" + _carouselArrowRight + "'></use></svg></button>"
            });

            _galleryControls.slick({
                infinite: true,
                arrows: false,
                fade: true,
                asNavFor: _gallery
            });


            $('.js-gallery--current').text( 1 );
            $('.js-gallery--total').text( _galleryItems.length );


            _gallery.on('beforeChange', function (event, slick, currentSlide) {

                if( _item_length_index == currentSlide) {
                    var _bgInject = $('.gallery-item[data-slick-index="1"]').attr('data-item');
                    console.log(_bgInject);
                    $('.slick-cloned[data-slick-index='+ _item_length +']').append('<div class="slideInject" style="background-image: url(' + _bgInject + '); "></div>');
                }
            });

            _gallery.on('afterChange', function (event, slick, currentSlide) {
                $('.js-gallery--current').text( currentSlide + 1 );
                $('.slick-cloned .media-overlay__item').remove();
                $(window).trigger('scroll');
                $('.slick-cloned').each(function() {
                    var $this  = $(this);
                    var $image = $this.attr('data-item');
                    $this.css('background-image', 'url("' + $image + '")');
                });
                $('.slideInject').remove();


            });


        }
    };
})(jQuery);
