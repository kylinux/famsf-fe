FAMSF = window.FAMSF || {};

(function ($) {
    FAMSF.eventsListing = {
        init: function () {
            var $dateFilter = $('.js-events-filter');
            var $dateLabel = $dateFilter.find('.js-events-filter--date');

            $dateFilter.on('click', '.js-events-filter--nav', function(e) {
                e.preventDefault();

                var currentDay = moment($dateLabel.text(), 'MMMM-D-YYYY');
                var queueDay = moment($dateLabel.text(), 'MMMM-D-YYYY');
                var direction = $(this).hasClass('js-events-filter--prev') ?
                    'previous' : 'next';

                if (direction === 'previous') {
                    queueDay = queueDay.subtract(1, 'days');
                }
                else if (direction === 'next') {
                    queueDay = queueDay.add(1, 'days');
                }

                $dateLabel.text(queueDay.format('MMMM D, YYYY'));

                if ($('.js-calendar').length) {
                    if (currentDay.format('M') !== queueDay.format('M')) {
                        FAMSF.calendar.navigate(queueDay, direction);
                    }
                    else {
                        var ymd = queueDay.format('YYYY-MM-DD');
                        $('.calendar-day-' + ymd).trigger('click');
                    }
                }
            });

            if (/^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
                $(window).on('load resize', function() {
                    FAMSF.eventsListing.computeBlockHeight();
                });
            }
        },

        updateListing: function(momentDate) {
            // TODO: Do AJAX call here.

            // Dummy data.
            var blocks = [
                {
                  "url": "#",
                  "image": "img/fpo-4x3-4.jpg",
                  "category": "Special Events",
                  "title": "&ldquo;Bouquets to Art&rdquo;: Elegant Catered Lunch",
                  "dateVenue": "12:00pm – 1:45pm, Piazzoni Murals Room"
                },
                {
                  "url": "#",
                  "image": "img/fpo-4x3-5.jpg",
                  "category": "Special Events",
                  "title": "&ldquo;Bouquets to Art&rdquo;: Lecture &amp; Demonstration by Francois Weeks",
                  "dateVenue": "2:00pm – 3:30pm, Wilsey Court"
                },
                {
                  "url": "#",
                  "image": "img/fpo-4x3-6.jpg",
                  "category": "Docent Tour",
                  "title": "In Pursuit of Excellence: American Decorative Arts",
                  "dateVenue": "12:00pm – 1:30pm, Wilsey Court"
                },
                {
                  "url": "#",
                  "category": "Docent Tour",
                  "title": "&ldquo;Three Masterpieces in 30 Minutes&rdquo;",
                  "dateVenue": "1:00pm – 1:30pm, Wilsey Court"
                },
                {
                  "url": "#",
                  "category": "Docent Tour",
                  "title": "Art and Architecture: Hightlights of the de Young",
                  "dateVenue": "2:00pm – 3:00pm, Wilsey Court"
                },
                {
                  "url": "#",
                  "category": "Docent Tour",
                  "title": "Colonial Through Contemporary",
                  "dateVenue": "11:00am – 12:00pm, The Rotunda"
                },
                {
                  "url": "#",
                  "category": "Docent Tour",
                  "title": "Beyond Western Africa, Oceania, and the Americas",
                  "dateVenue": "12:00pm – 12:30pm, The Rotunda"
                }
            ];

            // Shuffle an array.
            function shuffle (array) {
              var i = 0;
              var j = 0;
              var temp = null;

              for (i = array.length - 1; i > 0; i -= 1) {
                j = Math.floor(Math.random() * (i + 1));
                temp = array[i];
                array[i] = array[j];
                array[j] = temp;
              }

              return array;
            }

            blocks = shuffle(blocks);

            // Prepare dummy response.
            var count = Math.floor( Math.random() * blocks.length );
            var response = { 'blocks': blocks.slice(0, count) };

            // TODO: Put the codes below inside the AJAX success method.
            $('.events-listing-blocks .block--error').removeClass('visible');

            if (response.blocks.length) {
                $('.events-listing-blocks .block--item').remove();

                for (var i = 0; i < response.blocks.length; i++) {
                    var $content = $( $('#events-listing-blocks-template').html() );

                    if (response.blocks[i].image) {
                        $content.find('.block--media a')
                            .attr('data-img', response.blocks[i].image);
                    }
                    else {
                        $content.addClass('block--item--noimage');
                        $content.find('.block--media').remove();
                    }

                    $content.find('a').attr('href', response.blocks[i].url);
                    $content.find('.block--category').text(response.blocks[i].category);
                    $content.find('.block--title').html(response.blocks[i].title);
                    $content.find('.block--datevenue').text(response.blocks[i].dateVenue);

                    $('.events-listing-blocks .block--error').before($content);
                }

                FAMSF.blocks.createImages();
                if (/^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
                    FAMSF.eventsListing.computeBlockHeight();
                }
                FAMSF.calendar.updateWaypoints();
            }
            else {
                $('.events-listing-blocks .block--item').remove();
                $('.events-listing-blocks .block--error').addClass('visible');

                FAMSF.calendar.resetCalendar();
            }

            FAMSF.transition.refresh();
        },

        computeBlockHeight: function() {
            var $blocks = $('.events-listing-blocks .block--item');

            $blocks.removeAttr('style');
            $blocks.each(function(i) {
                $(this).height( $(this).height() );
            });
        }
    };
})(jQuery);
