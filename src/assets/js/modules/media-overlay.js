FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.mediaOverlay = {
    init: function () {
      var mo = FAMSF.mediaOverlay,
          $mediaOverlayItems,
          $dataAttributes = $('.data-attributes'),
          svgSpritePath = $dataAttributes.data('svgSpritePath'),
          carouselArrowLeft = svgSpritePath + '#' + $dataAttributes.data('carouselArrowLeft'),
          carouselArrowRight = svgSpritePath + '#' + $dataAttributes.data('carouselArrowRight'),
          overlayClose = svgSpritePath + '#' + $dataAttributes.data('overlayClose');

      if ($('main').is('.page--regular-exhibitions, .page--special-exhibitions')) {
        $mediaOverlayItems = $('.media-overlay__item');
      } else {
        $mediaOverlayItems = $('.media-overlay__item.video');
      }

      if ($mediaOverlayItems.length) {
        $mediaOverlayItems.on('click', function(e) {
          e.preventDefault();

          var $this = $(this),
              $mediaItems = $this.hasClass('media-overlay__item--gallery') ? $mediaOverlayItems.filter('.media-overlay__item--gallery') : $this,
              mediaItems  = $.map($mediaItems, function(n) {
                var data = $(n).data();
                data.type = data.type === 'video' ? 'iframe' : data.type;

                return data;
              });

          $.magnificPopup.open({
            alignTop: true,
            closeOnBgClick: false,
            closeMarkup: '<button title="Close button" type="button" class="mfp-close">%title%</button>',
            gallery: {
              enabled: true,
              arrowMarkup: '<button title="%dir% button" type="button" class="mfp-arrow mfp-arrow-%dir%">%title%</button>',
              tPrev: '<svg role="img"><use xlink:href="' + carouselArrowLeft + '"></use></svg>',
              tNext: '<svg role="img"><use xlink:href="' + carouselArrowRight + '"></use></svg>',
              tCounter: '%curr% / %total%',
              markup: '<div></div>'
            },
            items: mediaItems,
            mainClass: 'media-overlay',
            tClose: '<svg role="img"><use xlink:href="' + overlayClose + '"></use></svg>',
            type: 'image',
            image: {
              markup: '<div class="mfp-figure">'+
              '<div class="mfp-close"></div>'+
              '<figure>'+
              '<div class="mfp-img"></div>'+
              '<figcaption>'+
              '<div class="mfp-bottom-bar">'+
              '<div class="mfp-title"></div>'+
              '</div>'+
              '</figcaption>'+
              '</figure>'+
              '<div class="mfp-counter"></div>'+
              '</div>'
            },
            iframe: {
              markup: '<div class="mfp-iframe-container">'+
              '<div class="mfp-close"></div>'+
              '<div class="mfp-iframe-scaler">'+
              '<iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe>'+
              '</div>'+
              '<div class="mfp-title"></div>'+
              '<div class="mfp-counter"></div>'+
              '</div>',
              patterns: {
                youtube: {
                  index: 'youtube.com',
                  id: 'v=',
                  src: '//www.youtube.com/embed/%id%?autoplay=0'
                },
                vimeo: {
                  index: 'vimeo.com/',
                  id: '/',
                  src: '//player.vimeo.com/video/%id%?autoplay=0'
                }
              }
            },
            callbacks: {
              open: function(){
                $('html,body').css({
                  'overflow': 'hidden',
                  'position': 'relative'
                });
              },
              close: function(){
                $('html,body').removeAttr('style');
              },
              change: function(){
                //mo.mediaResize(this);
              },
              imageLoadComplete: function(){
                //mo.mediaResize(this);
              },
              updateStatus: function(data) {
                mo.mediaResize(this);
              },
              resize: function(data) {
                mo.mediaResize(this);
              }

            }
          }, $mediaItems.index($this));
        });

      }
    },
    mediaResize: function( mp ){
      var interval, $iframe, iframe = {};
      var $container      = mp.container; // mfp-container
      var $contentCont    = mp.contentContainer; // mfp-content
      var $content        = mp.content; // mfp-figure
      var $img            = $('img', $container); // mfp-img

      var newHeight       = function( objOffsetTop, captionHeight ) { return $container.height() - (objOffsetTop - $(document).scrollTop()) - captionHeight - 50; };



      clearInterval(interval);

      // for img
      if ( $img.length ) {
        $img.css({
          maxHeight : newHeight( $img.offset().top, $('.mfp-title', $container).outerHeight() ),
          opacity   : 1
        });
      }

      // for iframe
      if ( mp.currItem.type === "iframe" ) {
        interval = setInterval(function(){
          if ( $('iframe', $container).length ) {

            $iframe = $('.mfp-iframe-scaler', $container);
            iframe.H = newHeight( $iframe.offset().top, $('.mfp-title', $container).outerHeight() );
            iframe.W = 16/9*iframe.H;

            if ( iframe.W > $contentCont.width() ){
              iframe.W = $contentCont.width();
              iframe.H = 9/16*iframe.W;
            }

            $iframe.css({
              height: iframe.H,
              width: iframe.W,
              padding: 0,
              margin: '0 auto'
            });

            clearInterval(interval);
          }
        }, 200);

      }
    }
  };
})(jQuery);
