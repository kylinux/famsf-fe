FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.header = {
    init: function () {
      var self = this;

      this.$header      = $('.header');
      this.$headerTall  = $('.header--tall');
      this.$headerShort = $('.header--short');
      this.$navigation  = $('.navigation');

      // Search modal
      self.$header.find('.header__search-button').magnificPopup({
        alignTop: true,
        closeOnBgClick: false,
        items: {
          src:  '#header__search',
          type: 'inline'
        }
      });

      // Watermark check and toggle
      self.hideWatermark();

      // Header navigation toggle
      self.$header.on('click', '.header__nav-toggle', function(e) {
        var $this   = $(this);
        var $target = $($this.data('target'));
        
        if (!self.$navigation.hasClass('navigation--expanded')) {
          $this.addClass('header__nav-toggle--active');
          self.expandNavigation($target);
        } else {
          $this.removeClass('header__nav-toggle--active');
          self.retractNavigation();
        }
      });

      // Header navigation items
      self.$header.on('click', '.header__nav-item a', function(e) {
        var $this   = $(this);
        var $target = $($this.data('target'));

        if ($target.length) {
          e.preventDefault();

          if (!$target.hasClass('navigation__nav--active')) {
            $this
              .closest('.header__nav-item')
              .addClass('header__nav-item--active')
              .siblings('.header__nav-item--active')
              .removeClass('header__nav-item--active');

            self.expandNavigation($target);
          }
          else {
            $this
              .closest('.header__nav-item')
              .removeClass('header__nav-item--active');

            self.retractNavigation();
          }
        }
      });

      // Navigation links
      self.$navigation.on('click', '.navigation__nav-item a, .navigation__back-button', function(e) {
        var $this   = $(this);
        var $target = $($this.data('target'));

        if ($target.length) {
          e.preventDefault();

          $this
            .closest('.navigation__nav')
            .removeClass('navigation__nav--active');

          $target.addClass('navigation__nav--active');
        }
      });

      // Navigation close button
      self.$navigation.on('click', '.navigation__close-button', function(e) {
        self.retractNavigation();
      });

      // Dropdown toggle
      self.$navigation.on('click', '.navigation__dropdown-toggle', function(e) {
        $(this)
          .toggleClass('navigation__dropdown-toggle--active')
          .next('.navigation__dropdown')
          .slideToggle();
      });

      // Expand/retract short header
      if (self.$headerTall.length) {
        $(window).on('scroll', function() {
          if (!self.$navigation.hasClass('navigation--expanded')) {
            var headerOffset    = self.$headerTall.offset(),
                waypoint        = headerOffset.top + self.$headerTall.innerHeight(),
                windowScrolltop = $(window).scrollTop();

            self.$headerShort.toggleClass('header--expanded', windowScrolltop > waypoint);
            self.$navigation.find('.navigation__logo-container').toggleClass('is-hidden', windowScrolltop > waypoint);
          }
        }).trigger('scroll');
      }

      // Retract navigation on resize
      var prevBreakpoint = FAMSF.utils.getBreakpoint();
      $(window).on('resize', function() {
        var currentBreakpoint = FAMSF.utils.getBreakpoint();

        if (self.$navigation.hasClass('navigation--expanded')) {
          if (currentBreakpoint !== prevBreakpoint) {
            self.retractNavigation();
          }

          $('body').css('width', 'auto');
        }

        prevBreakpoint = currentBreakpoint;
      });
    },
    hideWatermark: function() {
      var self = this;

      if (self.$header.hasClass('header--expanded')) {
        self.$navigation.find('.navigation__logo-container').addClass('is-hidden');
      }
    },
    expandNavigation: function($target) {
      var self   = this;
      var navTop = this.$headerTall.innerHeight() - $(window).scrollTop() - 1;
      var $body  = $('body');

      if (self.$headerShort.hasClass('header--expanded') || self.$headerTall.is(':hidden')) {
        navTop = self.$headerShort.innerHeight() - 1;
      }

      if (self.$navigation.hasClass('navigation--expanded')) {
        $target
          .addClass('navigation__nav--active')
          .siblings('.navigation__nav--active')
          .removeClass('navigation__nav--active');
      }

      self.$navigation
        .removeClass('navigation--scrollable')
        .addClass('navigation--expanded')
        .css('top', navTop)
        .one('transitionend', function(e) {
          self.$navigation.addClass('navigation--scrollable');
          $target.addClass('navigation__nav--active');
        });

      var bodyWidth = $body.innerWidth();

      $body
        .css('width', bodyWidth)
        .addClass('no-scroll');
    },
    retractNavigation: function() {
      var self = this;

      self.$header
        .find('.header__nav-toggle--active, .header__nav-item--active')
        .removeClass('header__nav-toggle--active header__nav-item--active');

      self.$navigation
        .removeClass('navigation--scrollable')
        .removeClass('navigation--expanded')
        .one('transitionend', function(e) {
          self.$navigation
            .find('.navigation__nav--active')
            .removeClass('navigation__nav--active');

            $('body')
              .css('width', 'auto')
              .removeClass('no-scroll');
        });
    }
  };
})(jQuery);
