FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.mediaGallery = {
    init: function () {
      var $mediaGallery      = $('.media-gallery');
      var $mediaGalleryItems = $('.media-gallery__items', $mediaGallery);
      var mediaGalleryOptions = {
        peeking: false
      };

      new FAMSF.Slider($mediaGalleryItems, mediaGalleryOptions);
    }
  };
})(jQuery);
