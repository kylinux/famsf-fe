var FAMSF = {};

(function($) {

  FAMSF = {
    $document: $(document),
    $window: $(window),
    $body: $('body'),
    // All pages
    'common': {
      init: function () {
        // Events Handler
        svg4everybody();
        FAMSF.eventsHandler.init();
        FAMSF.mediaOverlay.init();

        if ($('.js-header').length)               { FAMSF.header.init(); }
        if ($('.js-ticker').length)               { FAMSF.ticker.init(); }
        if ($('.js-carousel-marquee').length)     { FAMSF.carouselMarquee.init(); }
        if ($('.js-manifesto').length)            { FAMSF.manifesto.init(); }
        if ($('.js-newsletter').length)           { FAMSF.newsletter.init(); }
        if ($('.js-map').length)                  { FAMSF.map.init(); }
        if ($('.js-permanent-collections').length){ FAMSF.permanentCollections.init(); }
        if ($('.js-media-gallery').length)        { FAMSF.mediaGallery.init(); }
        if ($('.js-cinematic-gallery').length)    { FAMSF.cinematicGallery.init(); }
        if ($('.js-editorial').length)            { FAMSF.editorial.init(); }
        if ($('.js-visit-notification').length)   { FAMSF.visitNotification.init(); }
        if ($('.js-body-module').length)          { FAMSF.bodymodule.init(); }
        if ($('.js-bio').length)                  { FAMSF.bio.init(); }
        if ($('.js-blocks').length)               { FAMSF.blocks.init(); }
        if ($('.hero--collection').length)        { FAMSF.collectionHero.init(); }
        if ($('.join-give-intro').length)         { FAMSF.joinGiveIntro.init(); }
        if ($('.js-membership-level').length)     { FAMSF.membershipLevel.init(); }
        if ($('.js-events-filter').length)        { FAMSF.eventsListing.init(); }
        if ($('.js-calendar').length)             { FAMSF.calendar.init(); }

        FAMSF.figures.init();
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        FAMSF.transition.init();
        FAMSF.drag.init();
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = FAMSF;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

  jQuery.fn.reverse = [].reverse;

})(jQuery); // Fully reference jQuery after this point.
