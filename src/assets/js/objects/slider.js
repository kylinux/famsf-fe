FAMSF = window.FAMSF || {};

FAMSF.Slider = function($el, options) {
  this.$el = $el;

  var $dataAttributes = $('.data-attributes'),
      svgSpritePath = $dataAttributes.data('svgSpritePath'),
      carouselArrowLeft = svgSpritePath + '#' + $dataAttributes.data('carouselArrowLeft'),
      carouselArrowRight = svgSpritePath + '#' + $dataAttributes.data('carouselArrowRight');

  var defaults = {
    autoplay: true,
    autoplaySpeed: 500000,
    dots: true,
    infinite: true,
    peeking: true,
    slidesToShow: 1,
    prevArrow: "<a href='#' class='slick-prev'><svg role='img'><use xlink:href='" + carouselArrowLeft + "'></use></svg></a>",
    nextArrow: "<a href='#' class='slick-next'><svg role='img'><use xlink:href='" + carouselArrowRight + "'></use></svg></a>",
    customPaging: function(slider, i) {
      return '<span class="counter">' + (i + 1) + '/' + slider.slideCount + '</span>';
    }
  };

  var settings = $.extend({}, defaults, options);

  if (settings.peeking) {
    $el.on('mouseenter', '.slick-arrow', function() {
      var $this    = $(this);
      var $current = $('.slick-current', $el);

      if ($this.hasClass('slick-next')) {
        $current
          .next('.slick-slide')
          .addClass('slick-slide--peeking slick-slide--peeking-next');
      }
      else {
        $current
          .prev('.slick-slide')
          .addClass('slick-slide--peeking slick-slide--peeking-prev');
      }
    });

    $el.on('mouseleave click', '.slick-arrow', function() {
      $('.slick-slide--peeking', $el)
        .removeClass('slick-slide--peeking-next slick-slide--peeking-prev')
        .one('transitionend', function() {
          $(this).removeClass('slick-slide--peeking');
        });
    });

    $el.on('afterChange', function (event, slick, currentSlide) {

      var $current = $('.slick-current', $el);

      $current
        .next('.slick-slide')
        .find('.media-bg')
        .lazyLoadXT({show: true});

      $current
        .prev('.slick-slide')
        .find('.media-bg')
        .lazyLoadXT({show: true});



      var $activeArrow = $('.slick-arrow', $el).filter(function() { return $(this).is(":hover"); });

      if ( $activeArrow.length ) {
        $activeArrow.trigger('mouseenter');
      }
    });

    $el.on('init', function (event, slick, currentSlide) {
      $('.slick-cloned', $el).each(function() {
        var $this  = $(this);
        var $image = $this.find('.media-bg');
        var src    = $image.data('bg');

        if (src) {
          $image.css('background-image', 'url("' + src + '")');
        }
      });
    });
  }

  $el.on('afterChange', function (event, slick, currentSlide) {
      $(window).trigger('scroll');
  });

  $el.on('beforeChange', function (event, slick, currentSlide) {
      console.log('before');

      var $current = $('.slick-current', $el);

      $current
        .prev('.slick-cloned')
        .css('background-image', 'url("' + $current.prev('.slick-cloned').data('bg') + '")')
        .removeAttr('data-bg');
      
      $current
        .next('.slick-cloned')
        .css('background-image', 'url("' + $current.next('.slick-cloned').data('bg') + '")')
        .removeAttr('data-bg');
  });

  $el.slick(settings);
};

FAMSF.Slider.prototype.destroy = function() {
  this.$el.slick('unslick');
};
