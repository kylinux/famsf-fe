FAMSF = window.FAMSF || {};

(function ($) {
    FAMSF.blocks = {
        init: function () {
            var Blocks = FAMSF.blocks;
            var _block = $('.close-block');
            _block.each(function () {
                $(this).on('click', function () {
                    if ($(this).is('.open')) {
                        $(this).removeClass('open');
                        $(this).parents('.block--text').removeClass('open');
                        $(this).siblings('.block--context').removeClass('open').slideUp(200);
                    } else {
                        $('.close-block.open').click();
                        $(this).addClass('open');
                        $(this).parents('.block--text').addClass('open');
                        $(this).siblings('.block--context').addClass('open').slideDown(200);
                    }
                });
            });
            Blocks.createImages();
            Blocks.loopImages();
            Blocks.enableLooping();
            FAMSF.eventHandler.subscribe('EVENT_RESIZE_INSTANT', Blocks.enableLooping);
        },
        createImages: function() {
            var $context = $('.js-blocks');
            var $block = $('.block--item', $context);

            $.each($block, function(){
                var $this = $(this);
                var $content = $('.content', $this);

                if ($content.length > 0) {
                  var images = $content.data('img').split(/,/).map(function(val){
                      if (val !== "") {
                          $('<span data-bg="'+ val +'" />').appendTo($content);
                      }
                  });

                  $('span:first', $content).addClass('is-active');
                  $('span:first', $content).lazyLoadXT();
                }
            });
        },
        loopImages: function() {
            var Blocks = this;
            var interval;
            var $context = $('.js-blocks');
            var $block = $('.block--item', $context);
            var $speed = 700;

            // events
            $block.on('mousehover mouseenter', function(){
                var i, bgImg,
                    $this = $(this),
                    $img = $('span', $this),
                    $firstImg = $img.first(),
                    $nextImg = $img.next(),
                    $imgCount = $img.length;

                if ( !$context.hasClass('loopImages') || $img.length === 1 ){
                    return;
                }

                clearInterval(interval);
                $img
                    .removeClass('is-active');

                $firstImg
                    .addClass('is-active');

                Blocks.imageLoadChecker($firstImg);

                $nextImg.css('left', 0);
                $img.lazyLoadXT();

                interval = setInterval(function(){
                    var $activeImg = $img.filter('.is-active:first'), src;
                    $nextImg = $activeImg.next().length ? $activeImg.next() : $img.first();

                    $(window).trigger('scroll');

                    //check next active image if fully loaded then add class .img-loaded
                    Blocks.imageLoadChecker($nextImg);

                    if ( !$nextImg.hasClass('img-loaded') ){
                        return;
                    }

                    $nextImg
                        .addClass('is-active')
                        .css('left', '');

                    $activeImg
                        .removeClass('is-active');

                    if ($nextImg.next().length){
                        $nextImg.next()
                            .css('left', 0);
                    }


                }, $speed);

            });

            $block.on('mouseleave', function(){
                var $this = $(this),
                    $img = $('span', $this);

                clearInterval(interval);
                $img
                    .removeClass('is-active')
                    .css('left', '');
                $img.first().addClass('is-active');

            });
        },
        enableLooping: function() {
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $('.js-blocks').removeClass('loopImages');
                return false;
            }
            if ( FAMSF.utils.getBreakpoint() === "small" || FAMSF.utils.getBreakpoint() === "medium" ){

                $('.js-blocks').removeClass('loopImages');
            } else {
                $('.js-blocks').addClass('loopImages');
            }
        },
        imageLoadChecker: function(img){
            if ( img.css('background-image') !== '' || !img.hasClass('img-loaded') ){
                var src = img.css('background-image').replace(/(^url\()|(\)$|[\"\'])/g, '');
                $('<img/>').attr('src', src).on('load', function() {
                    $(this).remove();
                    img.addClass('img-loaded');
                });
            }
        }
    };
})(jQuery);
