FAMSF = window.FAMSF || {};

(function($) {
  var dragIcon = {};
  var contain, xp, yp, wrapper;
  var mousedown = false;

  FAMSF.drag = {
    init: function() {
      new Dragdealer('horizontal-scroll', {
        horizontal: true,
        vertical: false,
        slide: true,
        loose: true
      });

      dragIcon.el = $('.grab-hover__follow');
      dragIcon.isActive = false;
      dragIcon.wasSeen = false;

      contain = $('.related-events');
      wrapper = $('.handle');
      xp = 0;
      yp = 0;

      FAMSF.drag.addMouseListeners();
    },

    removeDragIcon: function(){
      dragIcon.wasSeen = true;
      dragIcon.el.removeClass('active');
    },

    addMouseListeners: function() {
      var mousedown = false;
      var dragging = false;

      contain.on('mousedown', function(e) {

      if( dragIcon.isActive ){
        FAMSF.drag.removeDragIcon();
      }

      // if right mouse button
      if(e.button || e.ctrlKey){
        return;
      }

      mousedown = true;
      e.preventDefault();
    });

    $(window).on('mousemove', function(e) {
      if (dragIcon.isActive) {
          xp = e.clientX;
          yp = e.clientY;
      }

      dragIcon.el.css({left: xp, top: yp});

      if (!mousedown) { return; }

      dragging = true;
      mousedown = true;
    });

    wrapper.on('click', function(e){
      if (dragging) {
          e.preventDefault();
          e.stopPropagation();
      }

      FAMSF.drag.removeDragIcon();
    });

    wrapper.on('mouseenter', function(e){
      if (dragIcon.wasSeen) { return; }

      dragIcon.isActive = true;
      dragIcon.el.addClass('active');
    });

    wrapper.on('mouseleave', function(e){
      dragIcon.el.removeClass('active');
      dragIcon.isActive = false;
      });
    }
  };
})(jQuery);
