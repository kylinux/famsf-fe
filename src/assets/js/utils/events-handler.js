// BEGIN MODULE: EVENT HANDLER

FAMSF = window.FAMSF || {};

(function($) {
  FAMSF.eventsHandler = {
    init: function () {
      FAMSF.eventHandler = new SubPub();

      FAMSF.resizeHandler = function () {
        // SUB/PUB RESIZE
        var resizeObject = {};
        FAMSF.$window.on('resize', function () {
          resizeObject = {
            winWidth:   Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
            winHeight:  Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
          };

          FAMSF.eventHandler.publish('EVENT_RESIZE_INSTANT', resizeObject);
        });

        FAMSF.$window.on('resize', FAMSF.utils.debounce(function () {
          FAMSF.eventHandler.publish('EVENT_RESIZE', resizeObject);
        }, 200));
      };

      FAMSF.scrollHandler = function () {
        // SUB/PUB SCROLL
        var resizeObject = {};
        FAMSF.$window.on('scroll', function () {
          scrollObject = {
            scrollY:  Math.max(window.scrollY || document.documentElement.scrollTop),
            scrollX:  Math.max(window.scrollX || document.documentElement.scrollLeft)
          };

          FAMSF.eventHandler.publish('EVENT_SCROLL_INSTANT', scrollObject);
        });

        FAMSF.$window.on('scroll', FAMSF.utils.debounce(function () {
          FAMSF.eventHandler.publish('EVENT_SCROLL', scrollObject);
        }, 100));
      };

      // initialize each event handler
      FAMSF.resizeHandler();
      FAMSF.scrollHandler();
    }
  };
})(jQuery);

// END MODULE: EVENT HANDLER
