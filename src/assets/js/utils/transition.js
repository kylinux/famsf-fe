FAMSF = window.FAMSF || {};

(function($) {
  FAMSF.transition = {
    init: function() {
      $('.transition').waypoint(function(direction) {
        $(this.element).addClass('transition--completed');
          this.destroy(); // unbind
      }, {
        offset: '102%'
      });
    },
    refresh: function(){
      Waypoint.refreshAll();
      console.log('reset');
    }
  };
})(jQuery);
