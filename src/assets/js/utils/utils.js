FAMSF = window.FAMSF || {};

(function ($) {
  FAMSF.utils = {
    getBreakpoint: function() {
      var breakpoint = 'small';

      if (window.matchMedia('(min-width: 1401px)').matches) {
        breakpoint = 'xlarge';
      }
      else if (window.matchMedia('(min-width: 1001px)').matches) {
        breakpoint = 'large';
      }
      else if (window.matchMedia('(min-width: 721px)').matches) {
        breakpoint = 'medium';
      }

      return breakpoint;
    },

    debounce: function(func, wait, immediate) {
      var timeout;
      return function () {
        var context = this, args = arguments;
        var later = function() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    }
  };
})(jQuery);
