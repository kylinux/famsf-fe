// BEGIN MODULE: LAZY LOADING

FAMSF = window.FAMSF || {};

(function($) {
  FAMSF.lazyLoad = {
    init: function() {
      if ( !$('[data-src]').length ) { return; }

      $(document).find('[data-src]').lazyLoadXT();
    }
  };
})(jQuery);

// END MODULE: LAZY LOADING