.wysiwyg {
  p,
  ul,
  ol {
    @include respond(
      (margin-bottom, 26px, 26px, 28px)
    );

    &:last-child {
      @include respond(
        (margin-bottom, 0, null)
      );
    }
  }

  h2 {
    @include h4;
    @include respond(
      (margin-top, 38px, 38px, 48px),
      (margin-bottom, 26px, 26px, 28px)
    );

    color: color(text, secondary);
  }

  h3 {
    @include h8;
    @include respond(
      (margin-top, 38px, 48px),
      (margin-bottom, 26px, 20px)
    );
  }

  .link--external svg {
    display: inline-block;
    width: 13px;
    height: 13px;
    padding-left: 3px;
    transform: translateY(2px);
    stroke: currentColor;
    fill: none;
  }

  .link--download svg {
    display: inline-block;
    width: 30px;
    height: 13px;
    padding-right: 10px;
    transform: translateY(2px);
    stroke: currentColor;
    fill: none;
  }

  li {
    position: relative;
    @include respond(
      ('padding-left', 40px, 40px, 45px)
    );
    margin-bottom: 10px;

    li:first-of-type {
      margin-top: 10px; // spacing for nested lists
    }

    &:last-of-type {
      margin-bottom: 0;
    }
  }

  ul {
    ul {
      margin-bottom: 0;
    }

    li:before {
      content: '';
      display: block;
      position: absolute;
      top: 10px;
      left: 0;
      width: 30px;
      height: 0;
      border-top: 2px solid color(border, line);
    }
}

  ol {
    counter-reset: listCount;

    ol {
      margin-bottom: 0;
    }

    li {
      counter-increment: listCount;

      &:before {
        content: counter(listCount) ' — ';
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        color: $color-2;
        font-weight: 900;
      }

      ol li {
        &:before {
          content: counter(listCount, lower-alpha) ' — ';
        }

        ol li {
          &:before {
            content: counter(listCount) ' — ';
          }

          ol li {
            &:before {
                content: counter(listCount, lower-alpha) ' — ';
            }
          }
        }
      }
    }
  }
}
